-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 25, 2018 at 03:44 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_facility`
--

CREATE TABLE `tbl_facility` (
  `id` int(3) NOT NULL,
  `initial` varchar(4) NOT NULL,
  `fasilitas` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_facility`
--

INSERT INTO `tbl_facility` (`id`, `initial`, `fasilitas`) VALUES
(1, 'alq', 'Al-Quran'),
(2, 'saj', 'Sajadah'),
(3, 'kar', 'Karpet Sajadah'),
(4, 'wud', 'Air Wudhu'),
(5, 'muk', 'Mukenah'),
(6, 'sar', 'Sarung'),
(7, 'bar', 'Penitipan Barang'),
(8, 'lok', 'Loker Alas Kaki'),
(9, 'ac', 'AC'),
(10, 'kip', 'Kipas Angin'),
(11, 'toi', 'Toilet');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_management`
--

CREATE TABLE `tbl_management` (
  `id` int(11) NOT NULL,
  `initial` varchar(10) DEFAULT NULL,
  `jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_management`
--

INSERT INTO `tbl_management` (`id`, `initial`, `jabatan`) VALUES
(1, 'ketua', 'Ketua Masjid'),
(2, 'wakil', 'Wakil Ketua Masjid'),
(3, 'sekre', 'Sekretaris Masjid'),
(4, 'benda', 'Bendahara Masjid');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_masjid`
--

CREATE TABLE `tbl_masjid` (
  `id_masjid` int(11) NOT NULL,
  `nama_masjid` varchar(55) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `negara` int(6) NOT NULL,
  `provinsi` int(6) NOT NULL,
  `kabupaten_kota` int(6) NOT NULL,
  `kecamatan` int(6) NOT NULL,
  `kelurahan` int(6) NOT NULL,
  `alamat` text NOT NULL,
  `kategori` varchar(25) NOT NULL,
  `latitude` varchar(15) DEFAULT NULL,
  `longitude` varchar(15) DEFAULT NULL,
  `kode_pos` varchar(11) NOT NULL,
  `gedung` varchar(45) NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `email` varchar(55) NOT NULL,
  `web` varchar(55) NOT NULL,
  `twitter` varchar(55) NOT NULL,
  `fb` varchar(55) NOT NULL,
  `deskripsi` text NOT NULL,
  `rekening` varchar(25) NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_masjid`
--

INSERT INTO `tbl_masjid` (`id_masjid`, `nama_masjid`, `slug`, `negara`, `provinsi`, `kabupaten_kota`, `kecamatan`, `kelurahan`, `alamat`, `kategori`, `latitude`, `longitude`, `kode_pos`, `gedung`, `telepon`, `email`, `web`, `twitter`, `fb`, `deskripsi`, `rekening`, `tgl_input`) VALUES
(1, 'Masjidpedia', 'masjidpedia', 0, 0, 0, 0, 0, 'Jl. Sejiwa No.7, Karuwisi, Panakkukang, Kota Makassar, Sulawesi Selatan 90231, Indonesia', '', '-5.141104029766', ' 119.4387401481', '90123', '', '081242074226', 'masjid@gmail.com', 'www.facebook.com', 'mp', 'masjidpediamp', 'Hanya deskripsi biasa', '021123456789', '2018-04-19'),
(2, 'Masjid Nur Fisabilillah', 'masjid-nur-fisabilillah', 0, 0, 0, 0, 0, 'Jl. Enrekang 2, Sudiang Raya, Biring Kanaya, Kota Makassar, Sulawesi Selatan 90552, Indonesia', '', '-5.107956207140', ' 119.5355035206', '12345', '', '081212313414', 'nurfisabilillah@gmail.com', 'www.nurfisabilillah.com', 'nurfisabilillah', 'nurfisabilillah', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '12345678910', '2018-04-19'),
(3, 'Masjid Agung Makassar Al Markaz Al Islami', 'masjid-agung', 0, 0, 0, 0, 0, 'Jl. Al Markas Al Islami, Kalukuang, Tallo, Kota Makassar, Sulawesi Selatan 90213, Indonesia', '', '-5.129905344664', ' 119.4263483425', '12345', '', '0807 1 811 811', 'hanyalatihan@gmail.com', 'www.masjid.com', 'mpfb', 'mpfacebook', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '123456789', '2018-04-19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mosque`
--

CREATE TABLE `tbl_mosque` (
  `id` int(11) NOT NULL,
  `namamasjid` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `rekening` varchar(21) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `deskripsi` text,
  `input_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mosque`
--

INSERT INTO `tbl_mosque` (`id`, `namamasjid`, `slug`, `rekening`, `email`, `telepon`, `deskripsi`, `input_date`) VALUES
(1, 'Masjid Nur Fisabilillah', 'masjid-nur-fisabilillah', '0211234567890', 'masjid@gmail.com', '081242074226', 'Masjid yang dibangun berdasarkan swadaya masyarakat perumnas sudiang blok G.', '2018-05-06 14:16:49'),
(2, 'Masjid Jabal Nur', 'masjid-jabal-nur', '-', 'jabalnur@masjidpedia.com', '-', '-', '2018-06-30 16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mosque_to_facility`
--

CREATE TABLE `tbl_mosque_to_facility` (
  `id` int(11) NOT NULL,
  `id_mosque` int(11) DEFAULT NULL,
  `alquran` char(1) DEFAULT NULL,
  `sajadah` char(1) DEFAULT NULL,
  `karpet_sajadah` char(1) DEFAULT NULL,
  `air_wudhu` char(1) DEFAULT NULL,
  `mukenah` char(1) DEFAULT NULL,
  `sarung` char(1) DEFAULT NULL,
  `penitipan_barang` char(1) DEFAULT NULL,
  `loker_alaskaki` char(1) DEFAULT NULL,
  `ac` char(1) DEFAULT NULL,
  `kipas_angin` char(1) DEFAULT NULL,
  `toilet` char(1) DEFAULT NULL,
  `input_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mosque_to_facility`
--

INSERT INTO `tbl_mosque_to_facility` (`id`, `id_mosque`, `alquran`, `sajadah`, `karpet_sajadah`, `air_wudhu`, `mukenah`, `sarung`, `penitipan_barang`, `loker_alaskaki`, `ac`, `kipas_angin`, `toilet`, `input_date`) VALUES
(1, 1, 't', 't', 't', 't', 't', 't', 't', 't', 't', 't', 't', '2018-05-06 14:17:25'),
(2, 2, 't', 't', 't', 't', 't', 't', 'f', 'f', 'f', 'f', 't', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mosque_to_infaq`
--

CREATE TABLE `tbl_mosque_to_infaq` (
  `id` int(11) NOT NULL,
  `infaq_id` varchar(15) NOT NULL,
  `id_mosque` int(11) DEFAULT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `butuh` varchar(21) NOT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `detail_infaq` text NOT NULL,
  `tanggal_berakhir` date NOT NULL,
  `date_input` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mosque_to_infaq`
--

INSERT INTO `tbl_mosque_to_infaq` (`id`, `infaq_id`, `id_mosque`, `judul`, `butuh`, `deskripsi`, `detail_infaq`, `tanggal_berakhir`, `date_input`) VALUES
(1, '5b38a65340e0e', 1, 'Pembangunan menara masjid', '', '-', '', '2018-08-31', '2018-07-01 11:51:53'),
(2, '5b38a6607ff58', 2, 'Pembelian dan Pemasangan Keramik', '50000000', '-', '', '2018-09-28', '2018-07-01 11:51:41');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mosque_to_infaq_donasi`
--

CREATE TABLE `tbl_mosque_to_infaq_donasi` (
  `id` int(11) NOT NULL,
  `infaq_id` varchar(15) NOT NULL,
  `trans_id` varchar(10) NOT NULL,
  `nominal` varchar(15) NOT NULL,
  `nominal_mp` varchar(15) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `hambaAllah` tinyint(1) DEFAULT NULL,
  `dukungan` varchar(150) NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mosque_to_infaq_donasi`
--

INSERT INTO `tbl_mosque_to_infaq_donasi` (`id`, `infaq_id`, `trans_id`, `nominal`, `nominal_mp`, `nama`, `telepon`, `email`, `status`, `hambaAllah`, `dukungan`, `tgl_input`) VALUES
(1, '5b38a6607ff58', '583943', '200057', '10000', 'Ahmad Imron', '081242075335', 'aim.ahmad01@gmail.com', 1, NULL, '', '2018-07-09'),
(2, '5b38a6607ff58', '852987', '500076', '25000', 'Imron Ahmad', '081123456789', 'aim.ahmad01@gmail.com', 1, NULL, '', '2018-07-20'),
(3, '5b38a6607ff58', '902329', '250076', '12500', 'Imron Ahmad', '081123456789', 'aim.ahmad01@gmail.com', 0, NULL, '', '2018-07-09'),
(4, '5b38a6607ff58', '746627', '100061', '5000', 'Person', '081242075335', 'ahmadimron_aim@live.co.uk', 0, 0, '', '2018-07-19'),
(5, '5b38a6607ff58', '809946', '100073', '5000', 'Anto', '081123456789', 'ahmadimron_aim@yahoo.com', 1, 1, '', '2018-07-19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mosque_to_infaq_photo`
--

CREATE TABLE `tbl_mosque_to_infaq_photo` (
  `id` int(11) NOT NULL,
  `infaq_id` varchar(15) NOT NULL,
  `detail_photo` varchar(55) NOT NULL,
  `update_photo` varchar(55) NOT NULL,
  `date_input` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mosque_to_location`
--

CREATE TABLE `tbl_mosque_to_location` (
  `id` int(11) NOT NULL,
  `id_mosque` int(11) NOT NULL,
  `lat` varchar(20) DEFAULT NULL,
  `lng` varchar(20) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `input_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `location_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mosque_to_location`
--

INSERT INTO `tbl_mosque_to_location` (`id`, `id_mosque`, `lat`, `lng`, `description`, `input_date`, `location_status`) VALUES
(1, 1, '-5.147665', '119.432732', 'Jalan latihan saja untuk deskripsi lokasi masjid.', '2018-05-06 14:21:23', 1),
(2, 2, '-5.123592', '120.259630', 'Jalan Sultan Isma 2, Kelurahan Balangnipa', '2018-07-01 04:38:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mosque_to_management`
--

CREATE TABLE `tbl_mosque_to_management` (
  `id` int(11) NOT NULL,
  `id_mosque` int(11) DEFAULT NULL,
  `ketua` varchar(40) DEFAULT NULL,
  `wakil` varchar(40) DEFAULT NULL,
  `sekertaris` varchar(40) DEFAULT NULL,
  `bendahara` varchar(40) DEFAULT NULL,
  `periode` varchar(30) DEFAULT NULL,
  `input_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mosque_to_management`
--

INSERT INTO `tbl_mosque_to_management` (`id`, `id_mosque`, `ketua`, `wakil`, `sekertaris`, `bendahara`, `periode`, `input_date`) VALUES
(1, 1, 'Imron', 'Ahmad', 'Aim', 'Imbon', '2018-2019', '2018-05-06 14:22:00'),
(3, 2, '-', '-', '-', '-', '-', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mosque_to_photo`
--

CREATE TABLE `tbl_mosque_to_photo` (
  `id` int(11) NOT NULL,
  `id_mosque` int(11) DEFAULT NULL,
  `path_photo` varchar(60) DEFAULT NULL,
  `input_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mosque_to_sosmed`
--

CREATE TABLE `tbl_mosque_to_sosmed` (
  `id` int(11) NOT NULL,
  `id_mosque` int(11) DEFAULT NULL,
  `web` varchar(60) DEFAULT NULL,
  `fb` varchar(60) DEFAULT NULL,
  `twitter` varchar(60) DEFAULT NULL,
  `gplus` varchar(60) DEFAULT NULL,
  `youtube` varchar(60) DEFAULT NULL,
  `telegram` varchar(60) DEFAULT NULL,
  `bbm` varchar(25) DEFAULT NULL,
  `line` varchar(60) DEFAULT NULL,
  `wa` varchar(25) DEFAULT NULL,
  `input_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mosque_to_sosmed`
--

INSERT INTO `tbl_mosque_to_sosmed` (`id`, `id_mosque`, `web`, `fb`, `twitter`, `gplus`, `youtube`, `telegram`, `bbm`, `line`, `wa`, `input_date`) VALUES
(1, 1, '-', '-', '-', '-', '-', '-', '-', '-', '-', '2018-05-06 14:22:26'),
(2, 2, '', '', '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mosque_to_ulasan`
--

CREATE TABLE `tbl_mosque_to_ulasan` (
  `id` int(11) NOT NULL,
  `id_mosque` int(11) DEFAULT NULL,
  `ulasan` text,
  `photo` varchar(60) DEFAULT NULL,
  `input_by` int(11) DEFAULT NULL,
  `input_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mosque_to_value`
--

CREATE TABLE `tbl_mosque_to_value` (
  `id` int(11) NOT NULL,
  `id_mosque` int(11) DEFAULT NULL,
  `nilai` tinyint(1) DEFAULT NULL COMMENT '1 -> Like, 2->Standard, 3->Dislike',
  `input_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mosque_to_value`
--

INSERT INTO `tbl_mosque_to_value` (`id`, `id_mosque`, `nilai`, `input_date`) VALUES
(1, 1, 1, '2018-05-07 02:14:11');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sosmed`
--

CREATE TABLE `tbl_sosmed` (
  `id` int(3) NOT NULL,
  `initial` varchar(10) DEFAULT NULL,
  `sosmed` varchar(50) NOT NULL,
  `icon` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sosmed`
--

INSERT INTO `tbl_sosmed` (`id`, `initial`, `sosmed`, `icon`) VALUES
(1, 'web', 'Website', 'fas fa-globe'),
(2, 'fb', 'Facebook', 'fab fa-facebook'),
(3, 'tw', 'Twitter', 'fab fa-twitter'),
(4, 'yt', 'Youtube', 'fab fa-youtube'),
(5, 'gp', 'Google Plus', 'fab fa-google-plus'),
(6, 'tel', 'Telegram', 'fab fa-telegram'),
(7, 'line', 'Line', 'fab fa-line'),
(8, 'bbm', 'BBM', 'fab fa-blackberry'),
(9, 'wa', 'Whatsapp', 'fab fa-whatsapp');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(55) NOT NULL,
  `username` varchar(35) NOT NULL,
  `password` varchar(60) NOT NULL,
  `hp` varchar(15) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `email_code` varchar(123) DEFAULT NULL,
  `level` int(1) NOT NULL,
  `activated` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `nama`, `username`, `password`, `hp`, `email`, `email_code`, `level`, `activated`) VALUES
(1, 'Masjidpedia Admin', 'administrator', 'B4P9.YOVw2Xp6', NULL, NULL, NULL, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_facility`
--
ALTER TABLE `tbl_facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_management`
--
ALTER TABLE `tbl_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_masjid`
--
ALTER TABLE `tbl_masjid`
  ADD PRIMARY KEY (`id_masjid`);

--
-- Indexes for table `tbl_mosque`
--
ALTER TABLE `tbl_mosque`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_mosque_to_facility`
--
ALTER TABLE `tbl_mosque_to_facility`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Masjid` (`id_mosque`);

--
-- Indexes for table `tbl_mosque_to_infaq`
--
ALTER TABLE `tbl_mosque_to_infaq`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Masjid_Infaq` (`id_mosque`);

--
-- Indexes for table `tbl_mosque_to_infaq_donasi`
--
ALTER TABLE `tbl_mosque_to_infaq_donasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_mosque_to_infaq_photo`
--
ALTER TABLE `tbl_mosque_to_infaq_photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_mosque_to_location`
--
ALTER TABLE `tbl_mosque_to_location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Masjid_Location` (`id_mosque`);

--
-- Indexes for table `tbl_mosque_to_management`
--
ALTER TABLE `tbl_mosque_to_management`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Masjid_Manajemen` (`id_mosque`);

--
-- Indexes for table `tbl_mosque_to_photo`
--
ALTER TABLE `tbl_mosque_to_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Masjid_Photo` (`id_mosque`);

--
-- Indexes for table `tbl_mosque_to_sosmed`
--
ALTER TABLE `tbl_mosque_to_sosmed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Masjid_Sosmed` (`id_mosque`);

--
-- Indexes for table `tbl_mosque_to_ulasan`
--
ALTER TABLE `tbl_mosque_to_ulasan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Masjid_Ulasan` (`id_mosque`),
  ADD KEY `FK_User_Ulasan` (`input_by`);

--
-- Indexes for table `tbl_mosque_to_value`
--
ALTER TABLE `tbl_mosque_to_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Masjid_Value` (`id_mosque`);

--
-- Indexes for table `tbl_sosmed`
--
ALTER TABLE `tbl_sosmed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_facility`
--
ALTER TABLE `tbl_facility`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_management`
--
ALTER TABLE `tbl_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_masjid`
--
ALTER TABLE `tbl_masjid`
  MODIFY `id_masjid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_mosque`
--
ALTER TABLE `tbl_mosque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_mosque_to_facility`
--
ALTER TABLE `tbl_mosque_to_facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_mosque_to_infaq`
--
ALTER TABLE `tbl_mosque_to_infaq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_mosque_to_infaq_donasi`
--
ALTER TABLE `tbl_mosque_to_infaq_donasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_mosque_to_infaq_photo`
--
ALTER TABLE `tbl_mosque_to_infaq_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_mosque_to_location`
--
ALTER TABLE `tbl_mosque_to_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_mosque_to_management`
--
ALTER TABLE `tbl_mosque_to_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_mosque_to_photo`
--
ALTER TABLE `tbl_mosque_to_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_mosque_to_sosmed`
--
ALTER TABLE `tbl_mosque_to_sosmed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_mosque_to_ulasan`
--
ALTER TABLE `tbl_mosque_to_ulasan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_mosque_to_value`
--
ALTER TABLE `tbl_mosque_to_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_sosmed`
--
ALTER TABLE `tbl_sosmed`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_mosque_to_facility`
--
ALTER TABLE `tbl_mosque_to_facility`
  ADD CONSTRAINT `FK_Masjid` FOREIGN KEY (`id_mosque`) REFERENCES `tbl_mosque` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_mosque_to_infaq`
--
ALTER TABLE `tbl_mosque_to_infaq`
  ADD CONSTRAINT `FK_Masjid_Infaq` FOREIGN KEY (`id_mosque`) REFERENCES `tbl_mosque` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_mosque_to_location`
--
ALTER TABLE `tbl_mosque_to_location`
  ADD CONSTRAINT `FK_Masjid_Location` FOREIGN KEY (`id_mosque`) REFERENCES `tbl_mosque` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_mosque_to_management`
--
ALTER TABLE `tbl_mosque_to_management`
  ADD CONSTRAINT `FK_Masjid_Manajemen` FOREIGN KEY (`id_mosque`) REFERENCES `tbl_mosque` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_mosque_to_photo`
--
ALTER TABLE `tbl_mosque_to_photo`
  ADD CONSTRAINT `FK_Masjid_Photo` FOREIGN KEY (`id_mosque`) REFERENCES `tbl_mosque` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_mosque_to_sosmed`
--
ALTER TABLE `tbl_mosque_to_sosmed`
  ADD CONSTRAINT `FK_Masjid_Sosmed` FOREIGN KEY (`id_mosque`) REFERENCES `tbl_mosque` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_mosque_to_ulasan`
--
ALTER TABLE `tbl_mosque_to_ulasan`
  ADD CONSTRAINT `FK_Masjid_Ulasan` FOREIGN KEY (`id_mosque`) REFERENCES `tbl_mosque` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_User_Ulasan` FOREIGN KEY (`input_by`) REFERENCES `tbl_user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_mosque_to_value`
--
ALTER TABLE `tbl_mosque_to_value`
  ADD CONSTRAINT `FK_Masjid_Value` FOREIGN KEY (`id_mosque`) REFERENCES `tbl_mosque` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
