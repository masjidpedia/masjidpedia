<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Security extends MX_Controller {

	function __construct(){
		parent::__construct();
	}

	function make_hash($password)
	{
		$safe_pass = $this->awesome_hash($password);
		return $safe_pass;
	}

	function crypt_pass($password)
	{
		return crypt($password, 'B4sm4l1ah');
	}

	function make_md5($username)
	{
		$result_pass = $this->awesome_md5($username);
		return $result_pass;
	}

	function awesome_hash($password)
	{
		$new_pass = sha1($password);
		return $new_pass;
	}

	function awesome_md5($username)
	{
		$new_user = md5($username);
		return $new_user;
	}

	function make_sure_is_admin()
	{
		$user_id = $this->session->userdata('user_id_mp');
		$cek_username = Modules::run('common/check_one_data','tbl_user','id_user',$user_id);
		if($cek_username < 1){
			redirect(base_url().'login');
		}

		if($user_id == '' || empty($user_id))
		{
			redirect(base_url().'login');
		}
	}
}
