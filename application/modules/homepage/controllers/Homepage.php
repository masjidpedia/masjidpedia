<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage extends MX_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->load->view('landing_view');
	}

	function ourteam()
	{
		$this->load->view('teammasjidpedia_view');
	}

	function infaq()
	{
		$url = $this->uri->segment(4);
		$cek_infaq = Modules::run('common/check_one_data', 'tbl_mosque_to_infaq', 'infaq_id', $url);

		if($cek_infaq < 1){
			//echo "NAY";
			redirect('/');
		} else {
			$data['infaq_id'] = $url;
			$data['jlh_donatur'] = Modules::run('common/check_two_data', 'tbl_mosque_to_infaq_donasi', 'infaq_id', 'status', $url, 1);
			$data['dana_terkumpul'] = Modules::run('common/get_two_column','tbl_mosque_to_infaq_donasi', 'infaq_id', 'status', $url, 1);
			$this->load->view('InfaqDetail_view', $data);
		}
	}

	function back()
	{
		redirect('/');
	}

	function donasi()
	{
		//redirect('/');
		$url = $this->uri->segment(4);
		$slug = $this->uri->segment(2);

		$cek_infaq = Modules::run('common/check_one_data', 'tbl_mosque_to_infaq', 'infaq_id', $url);

		if($cek_infaq < 1){
			//echo "NAY";
			redirect('/');
		} else {
			$data['slug'] = $slug;
			$data['infaq_id'] = $url;
			$this->load->view('DonasiDetail_view', $data);
		}
	}

	function simpan_donasi()
	{
		$slug = $this->uri->segment(2);
		$url = $this->uri->segment(4);
		//echo $url;
		$cek_infaq = Modules::run('common/check_one_data', 'tbl_mosque_to_infaq', 'infaq_id', $url);
		if($cek_infaq < 1){
			redirect('/');
		} else {
			$this->load->library('email');

			$fullname = $this->input->post('fullname', TRUE);
			$hp = $this->input->post('hp', TRUE);
			$email = $this->input->post('email', TRUE);
			$hamba = $this->input->post('hamba', TRUE);
			$rp_mp = $this->input->post('rpmp', TRUE);
			$idtrans = $this->input->post('idtransval', TRUE);
			$nominaltrans = $this->input->post('nominaltrans', TRUE);

			//echo $hamba;
			if($hamba == 'on'){
				$ishamba = 1;
			} else {
				$ishamba = 0;
			}

			$data_donasi = array(
				'infaq_id' => $url,
				'trans_id' => $idtrans,
				'nominal' => $nominaltrans,
				'nominal_mp' => $rp_mp,
				'nama' => $fullname,
				'telepon' => $hp,
				'email' => $email,
				'hambaAllah' => $ishamba,
				'status' => 0,
				'tgl_input' => date('Y-m-d')
			);

			$detailmasjid = Modules::run('common/get_one_column', 'tbl_mosque', 'slug', $slug);
			foreach ($detailmasjid->result() as $masjidvalue) {
				$masjid_name = $masjidvalue->namamasjid;
			}

			$detailinfak = Modules::run('common/get_one_column', 'tbl_mosque_to_infaq', 'infaq_id', $url);
			foreach ($detailinfak->result() as $infakvalue) {
				$judulinfak = $infakvalue->judul;
			}
			

			$message = '<!DOCTYPE html><html><head><meta charset="UTF-8"></head><body>
            <p>Kepada '.$fullname.',</p>
            <p>Terima Kasih telah memberikan infak melalui website masjidpedia.com.</p><br/>

            <p>Detail Transaksi Infak</p>
            <p>Id Transaksi Infak : #'.$idtrans.'</p>
            <p>Metode Pembayaran : Transfer Bank</p><br/>

            <p style="color:red">Menunggu Konfirmasi Pembayaran</p>
            <p>Mohon melakukan konfirmasi pembayaran paling lambat 3 hari setelah email ini terkirim ke anda. Sehingga memudahkan kami dalam melakukan verifikasi terhadap pembayaran infak anda. Terima Kasih.</p>

            <table border="1">
            <tr style="padding:10px;">
            	<td style="padding:10px;">Infak</td>
            	<td style="padding:10px;">Nominal</td>
            </tr>
            <tr style="padding:10px;">
            	<td style="padding:10px;">'.$judulinfak.' '.$masjid_name.'</td>
            	<td style="padding:10px;">'.$nominaltrans.'</td>
            </tr>
            </table>

            
            <br/><p>Terima Kasih</p>
            <br/><p>Masjidpedia Team</p>';

            $infaqpesan = 'Konfirmasi Infak #'.$idtrans;

            $this->email->from('info@masjidpedia.com', 'Masjidpedia');
            $this->email->to($email);
            $this->email->subject($infaqpesan);
            $this->email->set_mailtype("html");
            $this->email->message($message);

             if($this->email->send()){
            	 Modules::run('common/insert_to_table','tbl_mosque_to_infaq_donasi',$data_donasi);
				 $this->session->set_flashdata('success','Data donasi anda berhasil di tambahkan, Silahkan melakukan transfer ke atm sesuai dengan nominal dan nomor transaksi anda, Terima Kasih.');
				 redirect('/masjid'.'/'.$slug);
                // Modules::run('common/_insert_by_table','jaka_user_change_email',$data_insert);
                // $this->session->set_flashdata('sukses', 'Berhasil mengupdate data dan mengirimkan link aktivasi ke email baru anda. Silahkan aktifkan email tersebut.');
                // redirect('edit/profil/'.$username);
             } else {
            	//Modules::run('common/_update_by_table','jaka_user',$user_id, $data_update);
            	 Modules::run('common/insert_to_table','tbl_mosque_to_infaq_donasi',$data_donasi);
                 $this->session->set_flashdata('error', 'Gagal mengirim konfirmasi link ke email anda, pastikan alamat email anda benar.');
                //redirect('edit/profil/'.$username);
                 redirect('/masjid'.'/'.$slug);
             }


			//Modules::run('common/insert_to_table','tbl_mosque_to_infaq_donasi',$data_donasi);
			//$this->session->set_flashdata('success','Data donasi anda berhasil di tambahkan, Silahkan melakukan transfer ke atm sesuai dengan nominal dan nomor transaksi anda, Terima Kasih.');
			//redirect('/masjid'.'/'.$slug);
		}
	}
}