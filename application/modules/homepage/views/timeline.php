<ul class="timeline">
                            <li>
                              <div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
                              <div class="timeline-panel">
                                <div class="timeline-heading">
                                  <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                                  <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 11 hours ago via Twitter</small></p>
                                </div>
                                <div class="timeline-body">
                                  <!-- <img style="margin:0 auto;" src="<?php echo base_url(); ?>/assets/img/additional/img-1.jpg" alt="First slide"> -->
                                  <div style="width: 100%;height:300px;background:red;margin-bottom:13px;"></div>
                                  <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                                </div>
                              </div>
                            </li>
                            <li class="timeline-inverted">
                              <div class="timeline-badge warning"><i class="glyphicon glyphicon-credit-card"></i></div>
                              <div class="timeline-panel">
                                <div class="timeline-heading">
                                  <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                                </div>
                                <div class="timeline-body">
                                  <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                                  <p>Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no mé, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet mé vel lectus scelerisque interdum cursus velit auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac mauris lectus, non scelerisque augue. Aenean justo massa.</p>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="timeline-badge danger"><i class="glyphicon glyphicon-credit-card"></i></div>
                              <div class="timeline-panel">
                                <div class="timeline-heading">
                                  <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                                </div>
                                <div class="timeline-body">
                                  <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                                </div>
                              </div>
                            </li>
                            <li class="timeline-inverted">
                              <div class="timeline-panel">
                                <div class="timeline-heading">
                                  <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                                </div>
                                <div class="timeline-body">
                                  <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="timeline-badge info"><i class="glyphicon glyphicon-floppy-disk"></i></div>
                              <div class="timeline-panel">
                                <div class="timeline-heading">
                                  <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                                </div>
                                <div class="timeline-body">
                                  <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                                  <hr>
                                  <div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                      <i class="glyphicon glyphicon-cog"></i> <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                      <li><a href="#">Action</a></li>
                                      <li><a href="#">Another action</a></li>
                                      <li><a href="#">Something else here</a></li>
                                      <li class="divider"></li>
                                      <li><a href="#">Separated link</a></li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="timeline-panel">
                                <div class="timeline-heading">
                                  <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                                </div>
                                <div class="timeline-body">
                                  <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                                </div>
                              </div>
                            </li>
                            <li class="timeline-inverted">
                              <div class="timeline-badge success"><i class="glyphicon glyphicon-thumbs-up"></i></div>
                              <div class="timeline-panel">
                                <div class="timeline-heading">
                                  <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                                </div>
                                <div class="timeline-body">
                                  <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                                </div>
                              </div>
                            </li>
                        </ul>




<div class="col-lg-2 mb-4">
                           <div class="card">
                              <h1 style="text-align: center;margin-top: 13px;" class="lead"><strong>Rp.300.000,-</strong></h1>
                              <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/unknown_person.jpg" alt="Card image cap">
                              <div style="text-align:center;" class="card-body">
                                <h5 style="margin-bottom:0px;" class="card-title">Ahmad Imron</h5>
                                <p style="margin-bottom:0px;">(Offline Donatur)</p>
                                <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock"></i> 29 Juni 2018</p>
                              </div>
                            </div>
                         </div>
                         <div class="col-lg-2 mb-4">
                           <div class="card">
                              <h1 style="text-align: center;margin-top: 13px;" class="lead"><strong>Rp.300.000,-</strong></h1>
                              <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/additional/img-1.jpg" alt="Card image cap">
                              <div style="text-align:center;" class="card-body">
                                <h5 style="margin-bottom:0px;" class="card-title">Contoh nama orang</h5>
                                <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock"></i> 29 Juni 2018</p>
                              </div>
                            </div>
                         </div>
                         <div class="col-lg-2 mb-4">
                           <div class="card">
                              <h1 style="text-align: center;margin-top: 13px;" class="lead"><strong>Rp.300.000,-</strong></h1>
                              <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/unknown_person.jpg" alt="Card image cap">
                              <div style="text-align:center;" class="card-body">
                                <h5 style="margin-bottom:0px;" class="card-title">Ahmad Imron</h5>
                                <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock"></i> 29 Juni 2018</p>
                              </div>
                            </div>
                         </div>
                         <div class="col-lg-2 mb-4">
                           <div class="card">
                              <h1 style="text-align: center;margin-top: 13px;" class="lead"><strong>Rp.300.000,-</strong></h1>
                              <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/unknown_person.jpg" alt="Card image cap">
                              <div style="text-align:center;" class="card-body">
                                <h5 style="margin-bottom:0px;" class="card-title">Ahmad Imron</h5>
                                <p style="margin-bottom:0px;">(Offline Donatur)</p>
                                <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock"></i> 29 Juni 2018</p>
                              </div>
                            </div>
                         </div>
                         <div class="col-lg-2 mb-4">
                           <div class="card">
                              <h1 style="text-align: center;margin-top: 13px;" class="lead"><strong>Rp.300.000,-</strong></h1>
                              <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/unknown_person.jpg" alt="Card image cap">
                              <div style="text-align:center;" class="card-body">
                                <h5 style="margin-bottom:0px;" class="card-title">Ahmad Imron</h5>
                                <p style="margin-bottom:0px;">(Offline Donatur)</p>
                                <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock"></i> 29 Juni 2018</p>
                              </div>
                            </div>
                         </div>
                         <div class="col-lg-2 mb-4">
                           <div class="card">
                              <h1 style="text-align: center;margin-top: 13px;" class="lead"><strong>Rp.300.000,-</strong></h1>
                              <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/unknown_person.jpg" alt="Card image cap">
                              <div style="text-align:center;" class="card-body">
                                <h5 style="margin-bottom:0px;" class="card-title">Ahmad Imron</h5>
                                <p style="margin-bottom:0px;">(Offline Donatur)</p>
                                <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock"></i> 29 Juni 2018</p>
                              </div>
                            </div>
                         </div>
                         <div class="col-lg-2 mb-4">
                           <div class="card">
                              <h1 style="text-align: center;margin-top: 13px;" class="lead"><strong>Rp.300.000,-</strong></h1>
                              <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/unknown_person.jpg" alt="Card image cap">
                              <div style="text-align:center;" class="card-body">
                                <h5 style="margin-bottom:0px;" class="card-title">Ahmad Imron</h5>
                                <p style="margin-bottom:0px;">(Offline Donatur)</p>
                                <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock"></i> 29 Juni 2018</p>
                              </div>
                            </div>
                         </div>
                         <div class="col-lg-2 mb-4">
                           <div class="card">
                              <h1 style="text-align: center;margin-top: 13px;" class="lead"><strong>Rp.300.000,-</strong></h1>
                              <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/unknown_person.jpg" alt="Card image cap">
                              <div style="text-align:center;" class="card-body">
                                <h5 style="margin-bottom:0px;" class="card-title">Ahmad Imron</h5>
                                <p style="margin-bottom:0px;">(Offline Donatur)</p>
                                <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock"></i> 29 Juni 2018</p>
                              </div>
                            </div>
                         </div>
                         <div class="col-lg-2 mb-4">
                           <div class="card">
                              <h1 style="text-align: center;margin-top: 13px;" class="lead"><strong>Rp.300.000,-</strong></h1>
                              <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/unknown_person.jpg" alt="Card image cap">
                              <div style="text-align:center;" class="card-body">
                                <h5 style="margin-bottom:0px;" class="card-title">Ahmad Imron</h5>
                                <p style="margin-bottom:0px;">(Offline Donatur)</p>
                                <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock"></i> 29 Juni 2018</p>
                              </div>
                            </div>
                         </div>
                         <div class="col-lg-2 mb-4">
                           <div class="card">
                              <h1 style="text-align: center;margin-top: 13px;" class="lead"><strong>Rp.300.000,-</strong></h1>
                              <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/unknown_person.jpg" alt="Card image cap">
                              <div style="text-align:center;" class="card-body">
                                <h5 style="margin-bottom:0px;" class="card-title">Ahmad Imron</h5>
                                <p style="margin-bottom:0px;">(Offline Donatur)</p>
                                <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock"></i> 29 Juni 2018</p>
                              </div>
                            </div>
                         </div>
                         <div class="col-lg-2 mb-4">
                           <div class="card">
                              <h1 style="text-align: center;margin-top: 13px;" class="lead"><strong>Rp.300.000,-</strong></h1>
                              <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/unknown_person.jpg" alt="Card image cap">
                              <div style="text-align:center;" class="card-body">
                                <h5 style="margin-bottom:0px;" class="card-title">Ahmad Imron</h5>
                                <p style="margin-bottom:0px;">(Offline Donatur)</p>
                                <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock"></i> 29 Juni 2018</p>
                              </div>
                            </div>
                         </div>
                         <div class="col-lg-2 mb-4">
                           <div class="card">
                              <h1 style="text-align: center;margin-top: 13px;" class="lead"><strong>Rp.300.000,-</strong></h1>
                              <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/unknown_person.jpg" alt="Card image cap">
                              <div style="text-align:center;" class="card-body">
                                <h5 style="margin-bottom:0px;" class="card-title">Ahmad Imron</h5>
                                <p style="margin-bottom:0px;">(Offline Donatur)</p>
                                <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock"></i> 29 Juni 2018</p>
                              </div>
                            </div>
                         </div>
                         <div class="col-lg-2 mb-4">
                           <div class="card">
                              <h1 style="text-align: center;margin-top: 13px;" class="lead"><strong>Rp.300.000,-</strong></h1>
                              <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/unknown_person.jpg" alt="Card image cap">
                              <div style="text-align:center;" class="card-body">
                                <h5 style="margin-bottom:0px;" class="card-title">Ahmad Imron</h5>
                                <p style="margin-bottom:0px;">(Offline Donatur)</p>
                                <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock"></i> 29 Juni 2018</p>
                              </div>
                            </div>
                         </div>





<div class="col-lg-8">
            <form action="<?php echo base_url(); ?>/donasi/save" method="POST" role="form">
              <div class="row">
                <!-- <div style="background:yellow;width:100%;min-height:300px;"></div> -->
                <div class="col-lg-6">
                  <div class="form-group">
                      <label for="masjidname">Nama Lengkap <strong style="color:red;">*</strong></label>
                      <input required name="fullname" required type="text" class="form-control form-control-sm" id="fullname" placeholder="Masukkan nama lengkap">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                      <label for="masjidname">Telepon <strong style="color:red;">*</strong></label>
                      <input required name="hp" required type="text" class="form-control form-control-sm" id="hp" placeholder="Masukkan nama telepon">
                  </div>
                </div>
                <div class="col-lg-12 mb-3">
                  <div class="form-group">
                      <label for="rek">Email Address <strong style="color:red;">*</strong></label>
                      <div class="input-group">
                          <div class="input-group-prepend">
                              <span class="input-group-text"> <i class="far fa-credit-card"></i> </span>
                          </div>
                          <input name="email" type="text" class="form-control form-control-sm" id="email" placeholder="Masukkan Alamat Email">
                      </div>
                  </div>
                </div>
                <div class="col-lg-12">
                  <p style="font-size:2em;text-align:center;" class="display-4">Jumlah Donasi</p>
                  <!-- <p>Masukkan jumlah donasi minimal Rp.10.000,- dengan kelipatan ribuan (Contoh: 15.000, 50.000).</p> -->
                </div>
                <div class="col-lg-9 mx-auto mb-4">
                  <div class="form-group">
                    <label for="rek">Jumlah Infak ke Masjid <strong style="color:red;">*</strong></label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> Rp. </span>
                        </div>
                        <input required name="rp" type="text" class="form-control form-control-sm" id="rp" placeholder="Masukkan Nominal">

                        <!-- <input name="test" type="text" class="form-control form-control-sm" id="test" placeholder="Masukkan Nominal"> -->
                    </div>
                  </div>
                  <div style="margin-bottom:13px;" class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="customCheck2">
                      <label style="color:;" class="custom-control-label" for="customCheck2">
                        Tampilkan sebagai Hamba Allah.
                      </label>
                  </div>
                </div>
                <div class="col-lg-9 mx-auto">
                  <div style="background:#fffcc6;padding:17px;" class="jumbotron">
                    <!-- <h1 class="display-4">Hello, world!</h1> -->
                    <!-- <div style="margin-bottom:13px;" class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="customCheck2">
                      <label style="color:;" class="custom-control-label" for="customCheck2">
                        Tampilkan sebagai Hamba Allah.
                      </label>
                    </div> -->
                    <div class="form-group">
                      <!-- <input checked type="radio" class="custom-control-input" id="customCheck1"> -->
                      <label style="color:;" class="" for="">
                        Saya juga ingin membantu meringankan operasional masjidpedia dalam menyalurkan dan mengupdate proyek donasi, melanjutkan proyek lain dan mengembangkan ide dan aplikasi masjidpedia agar seluruh masjid semakin makmur.
                      </label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                      <label class="custom-control-label" for="customRadioInline1">0%</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input checked type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                      <label class="custom-control-label" for="customRadioInline2">5%(Default)</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline3" name="customRadioInline1" class="custom-control-input">
                      <label class="custom-control-label" for="customRadioInline3">10%</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline4" name="customRadioInline1" class="custom-control-input">
                      <label class="custom-control-label" for="customRadioInline4">15%</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline5" name="customRadioInline1" class="custom-control-input">
                      <label class="custom-control-label" for="customRadioInline5">20%</label>
                    </div>
                    <div id="infakmasjidpedia" class="form-group">
                    <hr class="my-4">
                    <label for="rek">Jumlah Infak ke Masjidpedia</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> Rp. </span>
                        </div>
                        <input required name="rpmp" type="text" class="form-control form-control-sm" id="rpmp" placeholder="Masukkan Nominal">
                    </div>
                  </div>
                  </div>
                </div>

                <!-- <div class="col-lg-9 mx-auto mb-4">
                  <div class="form-group">
                    <label for="rek">Total Infak</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> Rp. </span>
                        </div>
                        <input disabled name="rp" value="000000" type="text" class="form-control form-control-lg" id="rp" placeholder="">
                    </div>
                  </div>
                </div> -->

                <!-- <div class="col-lg-9 mx-auto">
                  <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                      <h1 id="totalinfak" class="display-4"></h1>
                      <p id="descrpmp" class="lead"></p>
                    </div>
                  </div>
                </div>   -->

                <!-- <div class="col-lg-7 mx-auto">
                  <div class="form-group">
                    <label for="address">Dukungan anda <strong style="color:red;">(150 Karakter)</strong></label>
                      <textarea required id="address" name="alamat" placeholder="Masukkan Dukungan anda" class="form-control form-control-lg"></textarea>
                  </div>
                </div> -->
                <!-- <div class="col-lg-12">
                  <p style="font-size:2em;" class="display-4">Metode Pembayaran</p>
                  <p>Beberapa instruksi untuk melakukan donasi pada proyek infaq yang anda pilih.</p>
                </div> -->
                <!-- <div class="col-lg-12 mb-5">
                  <p style="margin-left:13px;" class="lead">
                      Mekanisme Donasi Via transfer :<br/>
                      <ul><li class="lead">Transfer  lngsung  via Bank Muamalat, No Rek. 820.000.1081 an Budirman (Founder Masjidpedia)<br/>
                      Mohon Transfer dengan kode 100 unik (contoh : Nominal 100rb dengan mentransfer 100.100) <br/>
                      Setelah transfer SMS WA ke 085242411466<br/>
                      Format: Nama#Nominal Donasi<br/></li><br/>

                    <li class="lead">Melalui <a href="https://bit.ly/2tVqs2G">https://bit.ly/2tVqs2G</a> (tdk perlu konfirmasi via WA).</li></ul>
                  </p>
                </div> -->

                <div class="col-lg-6 mx-auto mt-1 mb-5">
                  <button style="width: 100%;" type="submit" class="btn btn-dark btn-sm text-white">
                      Selanjutnya
                  </button>
                </div>
                
              </div>
            </form>
          </div>