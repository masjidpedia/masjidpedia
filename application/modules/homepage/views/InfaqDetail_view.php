<!DOCTYPE html>
<html>
<head>
  <title>Mp | Infaq Masjid</title>
  <?php $this->load->view('common/meta_view'); ?>
  <?php echo css('toastr.min.css'); ?>
  <?php echo css('masjid.css'); ?>
  <?php echo css('event.css'); ?>
</head>
<body>
  <?php 
    $cek_infaq = Modules::run('common/check_one_data', 'tbl_mosque_to_infaq', 'infaq_id', $infaq_id);
    if($cek_infaq < 1){
      redirect('/');
      exit();
    }
      
    $get_infaq = Modules::run('common/get_one_column', 'tbl_mosque_to_infaq', 'infaq_id', $infaq_id);
    foreach ($get_infaq->result() as $infaqvalue) {
      $get_masjid = Modules::run('common/get_one_column','tbl_mosque','id',$infaqvalue->id_mosque);
      foreach ($get_masjid->result() as $masjidvalue) {
    ?>
  <div class="container-fluid">
    <div class="row">
      <a class="text-danger" style="position:absolute;right:7px;top:5px;" href="<?php echo base_url(); ?>"><i style="font-size:1.5em;" class="fas fa-home p-3"></i></a>
      <h3 style="font-size:3em;" class="display-4 mx-auto mt-5"><?php echo $infaqvalue->judul; ?></h3>
      
    </div>
    <div class="row">
      <h6 class="lead mx-auto mb-5"><a href="<?php echo base_url(); ?>masjid/<?php echo $masjidvalue->slug; ?>">Masjid Jabal Nur Sinjai</a></h6>
    </div>

    <!-- <div class="row">
      <div class="mx-auto"><a href="<?php echo base_url(); ?>masjid/new" class="text-success"><i style="font-size:4em" class="fab fa-ussunnah"></i></a></div>
    </div> -->

    <div class="container">
        <div style="min-height:350px;" class="row mb-5 pb-5">
          <div class="col-lg-12">
            <div class="row mb-4">
              <!-- <div style="background:red;height:400px;width:100%;"></div> -->
              <?php $donasiterkumpul = 0; foreach($dana_terkumpul->result() as $masjiddana){ 
                  $donasiterkumpul = $donasiterkumpul + $masjiddana->nominal; }
                  $persen = round((($donasiterkumpul/150000000)*100),1);
                ?>
              <div class="col-lg-4">
                <div class="row">
                  <div style="background:;min-height:425px;width:100%;">
                    <div class="container">
                      <div class="jumbotron">
                        <span class="badge-lg badge-pill badge-secondary mr-1"><?php echo $persen; ?>%</span><span class="badge-lg badge-pill badge-danger">Terkumpul</span>
                        <h1 style="font-size:3em;" class="display-4">Rp.<span id="danasterkumpul"><?php echo $donasiterkumpul ?></span></h1>
                        <p class="lead">Target dana dari kegiatan ini yaitu <span class="badge badge-secondary">Rp.150.000.000,-</span>, dan hari berakhir penggalangan dana ini yaitu <span class="badge badge-success">60 Hari</span> lagi.</p>
                        <!-- <div class="progress bg-dark">
                          <div class="progress-bar bg-danger" role="progressbar" style="width: 0%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">75%</div>
                        </div> -->
                        <hr class="my-4">
                        <p>Bantuan kegiatan ini dengan cara mengklik tombol donasi di bawah. Semoga apa yang anda sumbangkan dilipatgandakan oleh Allah SWT.</p>
                        <a class="btn btn-primary btn-lg" href="<?php echo base_url(); ?>masjid/masjid-jabal-nur/infaq/<?php echo $infaqvalue->infaq_id; ?>/donasi" role="button">Donasi Sekarang</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php  ?>
              <div class="col-lg-8">
                <div class="row">
                  <div style="background:;min-height:150px;width:100%;">
                    <img style="text-align:center;margin:0 auto;width: 100%;border-radius: 10px;" src="<?php echo base_url(); ?>assets/img/infaq/infaq-5b38a6607ff58.jpg">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="row">
              <div style="background:;min-height:900px;width:100%;">
                <div class="col-md-12 text-center ">
                <nav class="nav-justified ">
                  <div class="nav nav-tabs " id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="pop1-tab" data-toggle="tab" href="#pop1" role="tab" aria-controls="pop1" aria-selected="true">Detail Kegiatan</a>
                    <!-- <a class="nav-item nav-link" id="pop2-tab" data-toggle="tab" href="#pop2" role="tab" aria-controls="pop2" aria-selected="false">Update <span style="position: absolute;margin-top:0px;margin-left:7px;" class="badge badge-info">New</span></a> -->
                    <a class="nav-item nav-link" id="pop2-tab" data-toggle="tab" href="#pop2" role="tab" aria-controls="pop2" aria-selected="false">Update</a>
                    <?php if($jlh_donatur > 0){ ?>
                        <a class="nav-item nav-link" id="pop3-tab" data-toggle="tab" href="#pop3" role="tab" aria-controls="pop3" aria-selected="false">Donatur <span style="position: absolute;margin-top:0px;margin-left:7px;" class="badge badge-warning"><?php echo $jlh_donatur; ?></span></a>
                    <?php } else { ?>
                        <a class="nav-item nav-link" id="pop3-tab" data-toggle="tab" href="#pop3" role="tab" aria-controls="pop3" aria-selected="false">Donatur</a>
                    <?php } ?>
                  </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                  <div style="text-align:left;margin-top:17px;" class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
                    <div class="row">
                        <!-- <div class="col-lg-12">
                          <div style="width: 100%;min-height:500px;background: black;"></div>
                        </div> -->
                        <div class="col-lg-12 mt-3 p-3">
                          <p style="text-indent:0.51in;" class="lead">
                            Bulan Ramadhan lalu merupakan ramadhan perdana  masyarakat sekitar Masjid Jabal Nur Polewali, melaksanakan sholat tarwih berjamaah pasca masjid ini berdiri setahun lalu. Khusyuk terasa menyelimuti para jamaah melaksanakan ibadah di Masjid yang  dapat menampung sekitar 500 jamaah ini.  Tidak hanya sholat 5 waktu, tarwih dan buka puasa bersama, masyarakat sekitar juga memberdayakan masjid sebagai tempat berkumpul dan bermusyawarah membahas beragam hal diwilayah mereka. Begitulah sepintas aktivitas yang ada di masjid ini. </p>

                            <div class="row mb-4">
                              <div class="col-lg-6">
                                <img style="text-align:center;margin:0 auto;width:100%;border-radius: 10px;" src="<?php echo base_url(); ?>assets/img/infaq/jabal-nur/1.jpg">
                              </div>
                              <div class="col-lg-6">
                                <img style="text-align:center;margin:0 auto;width:100%;border-radius: 10px;" src="<?php echo base_url(); ?>assets/img/infaq/jabal-nur/4.jpg">
                              </div>
                            </div>

                            <p style="text-indent:0.51in;" class="lead">Namun sayang, kondisi fisik masjid ini belumlah sempurna, para jamaah beribadah dan beraktivitas dalam masjid yang hanya beralas tikar dan karpet, tanpa fasilitas lantai keramik.  Kondisi ini diakibatkan dana pembangunan yang belum mencukupi berhubung pendanaan hanya mengandalkan  swadaya masyarakat sekitar. Oleh Karenanya @Masjidpedia ingin mengajak para donatur kaum mukmin yang #ringan berinfak untuk berpartisipasi dalam mewujudkan masjid jabal rahmah yang nyaman dan kondusif sebagai pusat ibadah dan pemberdayaan masyarakat sekitar. </p>

                            <!-- <div class="row mb-4">
                              <div class="col-lg-6">
                                <img style="text-align:center;margin:0 auto;width:100%;border-radius: 10px;" src="<?php echo base_url(); ?>assets/img/infaq/jabal-nur/2.jpg">
                              </div>
                              <div class="col-lg-6">
                                <img style="text-align:center;margin:0 auto;width:100%;border-radius: 10px;" src="<?php echo base_url(); ?>assets/img/infaq/jabal-nur/5.jpg">
                              </div>
                            </div> -->
                            <!-- <div class="col-lg-12">
                              <img style="text-align:center;margin:0 auto;width:100%;border-radius: 10px;" src="<?php echo base_url(); ?>assets/img/infaq/jabal-nur/2.jpg">
                            </div> -->

                            <p style="text-indent:0.51in;" class="lead">Partisipasi para donatur dapat diwujudkan dengan mekanisme 1 orang donatur minimal menyediakan  2pcs lantai keramik ukuran 60x60cm senilai Rp. 100.000,- yang  dapat digunakan <strong>1 orang jamaah  sebagai tempat sholat mereka seumur hidup selama masjid tersebut digunakan</strong>. Bisa dibayangkan amal jariah yang mengalir kepada anda minimal 5 kali sehari disetiap shalat fardhu. Allahu akbar! Jumlah yang tentunya tak seberapa dibandingkan pahala yang akan diraih insyaAllah. <p>
                                

                            <p class="lead">‘Perumpamaan (nafkah yang dikeluarkan oleh) orang-orang yang menafkahkan hartanya di jalan Allah adalah serupa dengan sebutir benih yang menumbuhkan tujuh bulir, pada tiap-tiap bulir seratus biji. Allah melipat gandakan (ganjaran) bagi siapa yang Dia kehendaki. Dan Allah Maha Luas (karunia-Nya) lagi Maha Mengetahui.‘  (TQS Al- Baqarah (2) : 261) </p>

                            <!-- <p class="lead"> Mari berlomba dalam kebaikan mewujudkan fasilitas ibadah bagi sudara-saudara kita yang membutuhkan. Semoga Allah menambah keberkahan hidup bagi kita semua… Aamin..</p> -->

                            <p class="lead">
                              Mari berlomba dalam kebaikan mewujudkan fasilitas ibadah bagi sudara-saudara kita yang membutuhkan. Semoga Allah menambah keberkahan hidup bagi kita semua… Aamin..<br/><br/>
                              
                                Kelebihan Proyek Infak akan disalurkan ke masjid lain yg juga butuh keramik, Jika Anda menemukan masjid yang membutuhkan keramik, mohon sampaikan datanya via nomor WA (085242411466).<br/><br/>

                              Klik tombol DONATE untuk melakukan donasi,<br/>
                              Raih pahala sebesar 250rb dengan klik tombol SHARE
                            </p>
                            
                            <a class="btn btn-success btn-lg mr-2 mb-2" href="<?php echo base_url(); ?>masjid/masjid-jabal-nur/infaq/<?php echo $infaqvalue->infaq_id; ?>/donasi" role="button">Donasi Sekarang</a>

                            <!-- <a class="btn btn-primary btn-lg" href="#" role="button"><i class="fab fa-facebook mr-2"></i> Share</a> -->
                            <button type="button" class="btn btn-primary btn-lg mb-2" data-toggle="modal" data-target="#shareModal"><i class="fab fa-facebook mr-2"></i> Share</button>

                            <!-- <p class="lead">Notes : </p>
                            <p style="margin-left:13px;" class="lead">
                              Mekanisme Donasi Via transfer :<br/>
                              <ul><li class="lead">Transfer  lngsung  via Bank Muamalat, No Rek. 820.000.1081 an Budirman (Founder Masjidpedia)<br/>
                              Mohon Transfer dengan kode 100 unik (contoh : Nominal 100rb dengan mentransfer 100.100) <br/>
                              Setelah transfer SMS WA ke 085242411466<br/>
                              Format: Nama#Nominal Donasi<br/></li><br/>
                              Raih pahala sebesar 250rb dengan klik tombol SHARE

                            <li class="lead">Melalui <a href="https://bit.ly/2tVqs2G">https://bit.ly/2tVqs2G</a> (tdk perlu konfirmasi via WA).</li></ul>
                          </p> -->
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"><i class="fab fa-facebook mr-2"></i>Share Facebook</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <p class="lead">Konten :</p>
                                <p class="lead" id="copitext1">Bantu proyek infak pembelian dan pemasangan keramik pada Masjid Jabal Nur. Dengan mengakses link dibawah ini. <br/> <strong>https://bit.ly/2tVqs2G</strong></p>
                                

                                <!-- <textarea class="form-control mb-3" id="copitext">Bantu proyek infak pembelian dan pemasangan keramik pada Masjid Jabal Nur. Dengan mengakses link dibawah ini. https://bit.ly/2tVqs2G</textarea> -->

                                <p style="color:gray;font-size:1em;" class="lead">Klik tombol copy dibawah, dan paste pada status facebook anda.</p>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary btn-share-f" data-clipboard-target="#copitext1">Copy</button>

                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                        
                  </div>
                  <div style="text-align: left;margin-top:17px;" class="tab-pane fade" id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
                       <div class="row">
                        <p class="lead">Belum ada update terbaru.</p>
                       </div>
                  </div>
                  <div style="text-align: left;margin-top:17px;" class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
                       <div class="row">
                        <?php if($jlh_donatur < 1){ ?>
                         <p class="lead">Belum ada data terbaru.</p>
                        <?php } else {
                          $detaildonatur = Modules::run('common/get_two_column_order_by', 'tbl_mosque_to_infaq_donasi', 'infaq_id','status', $infaq_id, 1, 'tgl_input', 'DESC');
                          foreach ($detaildonatur->result() as $donaturvalue) {
                          ?>
                          <div class="col-lg-2 mb-4">
                             <div class="card">
                                <h1 style="text-align: center;margin-top: 13px;" class="lead">Rp.<strong class="donasirp"><?php echo $donaturvalue->nominal; ?></strong></h1>
                                <img style="text-align:center;margin:0 auto;margin-top: 13px;" width="70px" src="<?php echo base_url(); ?>assets/img/unknown_person.jpg" alt="Card image cap">
                                <div style="text-align:center;" class="card-body">
                                  <h5 style="margin-bottom:0px;" class="card-title"><?php if($donaturvalue->hambaAllah == 1){ echo 'Hamba Allah'; } else { echo $donaturvalue->nama; } ?></h5>
                                  <!-- <p style="margin-bottom:0px;">(Offline Donatur)</p> -->
                                  <p style="font-size: .81em;" class="card-text"><i class="fas fa-clock mr-2"></i><?php echo $donaturvalue->tgl_input; ?></p>
                                </div>
                              </div>
                          </div>
                        <?php } } ?>
                       </div>

                  </div>
                </div>
            </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  <?php } } ?>
  <?php $this->load->view('common/footer_test_view'); ?>
  <?php $this->load->view('common/js_view'); ?>
  <?php echo js('jquery.mask.js'); ?>
  <?php echo js('clipboard.min.js'); ?>
  <?php echo js('toastr.min.js'); ?>
  <script type="text/javascript">
    // new ClipboardJS('.btn-share-f', {
    //     container: document.getElementById('modal')
    // });

    var clipboard = new ClipboardJS('.btn-share-f', {
        container: document.getElementById('modal')
    });

    clipboard.on('success', function(e) {
        console.log(e);
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });

    $(function() {
        $('.donasirp').mask('000.000.000.000.000', {reverse: true});
        $('#danasterkumpul').mask('000.000.000.000.000', {reverse: true});
    });
  </script>
</body>
</html>