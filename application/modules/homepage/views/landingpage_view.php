<!DOCTYPE html>
<html>
<head>
  <title>Mp</title>
  <?php $this->load->view('common/meta_view'); ?>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
          <i style="font-size:1.20em;" class="fas fa-ellipsis-v py-1"></i>
        </button>
        <a style="font-size:2em;" class="navbar-brand text-white mx-auto" href="<?php echo base_url(); ?>">MP</a>
      
        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item px-3">
                <a class="nav-link" href="<?php echo base_url(); ?>masjid">Masjid <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item px-3">
                <a class="nav-link" href="#">Donasi</a>
              </li>
              <li class="nav-item px-3">
                <a class="nav-link" href="<?php echo base_url(); ?>login">Masuk</a>
              </li>
              <li class="nav-item px-3">
                <a class="nav-link" href="<?php echo base_url(); ?>register">Daftar</a>
              </li>
              <!-- <li class="nav-item px-3 dropdown">
                <a href="#" class="nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Profil <i style="font-size:.81em;" class="fas fa-ellipsis-v pl-2"></i></a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Second menu</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Three menu</a>
                </div>
              </li>
              <li class="nav-item px-3">
                <a class="nav-link text-warning" href="#"><i style="font-size:1.5em;" class="fas fa-power-off p-0 m-0"></i></a>
              </li> -->
            </ul>
        </div>
      </div>
  </nav>
  <section>
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div style="height:500px;"></div>
      </div>
      <!-- <div class="col-sm-8">
        <div class="row">
          <div style="height:500px;background:red;width:100%;">Kiri</div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="row">
          <div class="col-sm-12">
            <div class="row">
              <div style="height:250px;background:green;width:100%;">Kanan atas</div>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="row">
              <div style="height:250px;background:yellow;width:100%;">Kanan bawah</div>
            </div>
          </div>
        </div>
      </div> -->
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="row">
          <div class="pb-5 bg-light" style="min-height:500px;width:100%">
            <div class="container">
              <div class="row pt-5 pb-5">
                <div class="col-lg-12 text-center">
                  <h1 style="font-size:2.5em; color:#666768;font-family: calibri;">Kegiatan Infaq</h1>
                  <h5 style="color:#666768;">Membantu penggalangan dana masjid, amal jariyah.</h5>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-3">
                  <a style="text-decoration: none;" href="<?php echo base_url(); ?>detail"><div class="card">
                    <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/additional/noimg-menara.jpg" alt="Card image cap">
                    <div class="card-body">
                      <h6 style="font-size:.81em;" class="card-subtitle mb-2 text-muted">Masjidpedia Team <i style="color:#00e500;" class="fas fa-check-circle"></i></h6>
                      <h5 style="font-size:1em;" class="card-title">Pembangunan Menara Masjid</h5>

                      <p style="font-size:.81em;" class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                  </div></a>
                </div>

                <div class="col-lg-3">
                  <a style="text-decoration: none;" href="<?php echo base_url(); ?>detail"><div class="card">
                    <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/additional/noimg-menara.jpg" alt="Card image cap">
                    <div class="card-body">
                      <h6 style="font-size:.81em;" class="card-subtitle mb-2 text-muted">Masjidpedia Team <i style="color:#00e500;" class="fas fa-check-circle"></i></h6>
                      <h5 style="font-size:1em;" class="card-title">Pembangunan Menara Masjid</h5>

                      <p style="font-size:.81em;" class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                  </div></a>
                </div>

                <div class="col-lg-3">
                  <a style="text-decoration: none;" href="<?php echo base_url(); ?>detail"><div class="card">
                    <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/additional/noimg-menara.jpg" alt="Card image cap">
                    <div class="card-body">
                      <h6 style="font-size:.81em;" class="card-subtitle mb-2 text-muted">Masjidpedia Team <i style="color:#00e500;" class="fas fa-check-circle"></i></h6>
                      <h5 style="font-size:1em;" class="card-title">Pembangunan Menara Masjid</h5>

                      <p style="font-size:.81em;" class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                  </div></a>
                </div>

                <div class="col-lg-3">
                  <a style="text-decoration: none;" href="<?php echo base_url(); ?>detail"><div class="card">
                    <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/additional/noimg-menara.jpg" alt="Card image cap">
                    <div class="card-body">
                      <h6 style="font-size:.81em;" class="card-subtitle mb-2 text-muted">Masjidpedia Team <i style="color:#00e500;" class="fas fa-check-circle"></i></h6>
                      <h5 style="font-size:1em;" class="card-title">Pembangunan Menara Masjid</h5>

                      <p style="font-size:.81em;" class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                  </div></a>
                </div>

              </div>  
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row pt-5 mb-4">
      <div class="col-sm-12">
        <div class="row justify-content-center">
          <!-- <div style="background:;height:500px;width:100%"> -->
            
            <div class="col-lg-9">
            <div class="card-columns">
              <div class="card p-3">
                <blockquote class="blockquote mb-0 card-body">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                  <footer class="blockquote-footer">
                    <small class="text-muted">
                      Someone famous in <cite title="Source Title">Source Title</cite>
                    </small>
                  </footer>
                </blockquote>
              </div>
              <div class="card bg-primary text-white text-center p-3">
                <blockquote class="blockquote mb-0">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat.</p>
                  <footer class="blockquote-footer">
                    <small>
                      Someone famous in <cite title="Source Title">Source Title</cite>
                    </small>
                  </footer>
                </blockquote>
              </div>
              <div class="card text-center">
                <div class="card-body">
                  <h5 class="card-title">Card title</h5>
                  <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                  <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
              </div>
              <div class="card p-3 text-right">
                <blockquote class="blockquote mb-0">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                  <footer class="blockquote-footer">
                    <small class="text-muted">
                      Someone famous in <cite title="Source Title">Source Title</cite>
                    </small>
                  </footer>
                </blockquote>
              </div>
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Card title</h5>
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                  <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
              </div>
            </div>
            </div>

          <!-- </div> -->
        </div>
      </div>
    </div>
  </div>
      
    
  </section>
  <?php $this->load->view('common/footer_view'); ?>
  <?php $this->load->view('common/js_view'); ?>
</body>
</html>