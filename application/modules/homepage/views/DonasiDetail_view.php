<!DOCTYPE html>
<html>
<head>
  <title>Mp | Donasi Masjid</title>
  <?php $this->load->view('common/meta_view'); ?>
  <?php echo css('toastr.min.css'); ?>
  <?php echo css('masjid.css'); ?>
  <?php echo css('event.css'); ?>
  <?php echo css('smart_wizard.css'); ?>
</head>
<body>
  <?php 
    $cek_infaq = Modules::run('common/check_one_data', 'tbl_mosque_to_infaq', 'infaq_id', $infaq_id);
    if($cek_infaq < 1){
      redirect('/');
      exit();
    }
      
    $get_infaq = Modules::run('common/get_one_column', 'tbl_mosque_to_infaq', 'infaq_id', $infaq_id);
    foreach ($get_infaq->result() as $infaqvalue) {
      $get_masjid = Modules::run('common/get_one_column','tbl_mosque','id',$infaqvalue->id_mosque);
      foreach ($get_masjid->result() as $masjidvalue) {
    ?>
  <div class="container-fluid">
    <div class="row">
      <a class="text-danger" style="position:absolute;right:7px;top:5px;" href="<?php echo base_url(); ?>"><i style="font-size:1.5em;" class="fas fa-home p-3"></i></a>
      <h3 style="font-size:3em;" class="display-4 mx-auto mt-5">INFAK</h3>
    </div>
    <div class="row">
      <h3 style="font-size:2em;text-align:center;" class="display-4 mx-auto"><?php echo $infaqvalue->judul; ?></h3>
    </div>
    <div class="row">
      <h6 class="lead mx-auto mb-5"><a href="<?php echo base_url(); ?>masjid/<?php echo $masjidvalue->slug; ?>">Masjid Jabal Nur</a></h6>
    </div>

    <div class="container">
        <div style="min-height:350px;" class="row justify-content-md-center mb-5 pb-5">
          <div class="col-lg-7">
            <div id="smartwizard">
              <ul style="">
                  <li><a href="#step-1">Langkah 1<br /><small>Informasi Infak</small></a></li>
                  <li><a href="#step-2">Langkah 2<br /><small>Metode Pembayaran</small></a></li>
                  <!-- <li><a href="#step-3">Step 3<br /><small>This is step description</small></a></li> -->
                  <!-- <li><a href="#step-4">Step 4<br /><small>This is step description</small></a></li> -->
              </ul>

            <div>
                <div id="step-1" class="">
                    <!-- <h3 class="border-bottom border-gray pb-2">Informasi Infak</h3> -->
                    <div class="col-lg-12">
                      <!-- <div style="width:100%;background: red;height:300px;"></div> -->
                      <form id="myForm" action="<?php echo base_url(); ?>masjid/<?php echo $slug ?>/infaq/<?php echo $infaq_id ?>/simpan" method="POST" role="form">
                        <div class="row">
                          <!-- <div style="background:yellow;width:100%;min-height:300px;"></div> -->
                          <div class="col-lg-6">
                            <div id="form-step-0" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label for="fullname">Nama Lengkap <strong style="color:red;">*</strong></label>
                                  <input required name="fullname" required type="text" class="form-control form-control-sm" id="fullname" placeholder="Masukkan nama lengkap">
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div id="form-step-1" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label for="hp">Telepon <strong style="color:red;">*</strong></label>
                                  <input required name="hp" required type="text" class="form-control form-control-sm" id="hp" placeholder="Masukkan nama telepon">
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-12 mb-3">
                            <div id="form-step-2" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label for="email">Email Address <strong style="color:red;">*</strong></label>
                                  <div class="input-group">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text"> <i class="far fa-credit-card"></i> </span>
                                      </div>
                                      <input required name="email" type="email" class="form-control form-control-sm" id="email" placeholder="Masukkan Alamat Email">
                                  </div>
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <p style="font-size:2em;text-align:center;" class="display-4">Jumlah Donasi</p>
                            <!-- <p>Masukkan jumlah donasi minimal Rp.10.000,- dengan kelipatan ribuan (Contoh: 15.000, 50.000).</p> -->
                          </div>
                          <div class="col-lg-12 mx-auto mb-4">
                            <div id="form-step-3" role="form" data-toggle="validator">
                              <div class="form-group">
                                <label for="rek">Jumlah Infak ke Masjid <strong style="color:red;">*</strong></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> Rp. </span>
                                    </div>
                                    <input required name="rp" type="text" class="form-control form-control-sm" id="rp" placeholder="Masukkan Nominal">

                                    <input name="test" type="hidden" class="form-control form-control-sm" id="test" placeholder="Masukkan Nominal">
                                </div>
                                <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                            </div>
                            <div style="margin-bottom:13px;" class="custom-control custom-checkbox">
                                <input name="hamba" type="checkbox" class="custom-control-input" id="customCheck2">
                                <label style="color:;" class="custom-control-label" for="customCheck2">
                                  Tampilkan sebagai Hamba Allah.
                                </label>
                            </div>
                          </div>
                          <div class="col-lg-12 mx-auto">
                            <div style="background:#fffcc6;padding:17px;" class="jumbotron">
                              <!-- <h1 class="display-4">Hello, world!</h1> -->
                              <!-- <div style="margin-bottom:13px;" class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck2">
                                <label style="color:;" class="custom-control-label" for="customCheck2">
                                  Tampilkan sebagai Hamba Allah.
                                </label>
                              </div> -->
                              <div style="margin-bottom: 0px;" class="form-group">
                                <!-- <input checked type="radio" class="custom-control-input" id="customCheck1"> -->
                                <label style="color:;" class="" for="">
                                  Saya juga ingin membantu meringankan operasional masjidpedia (sebesar 5%) dalam menyalurkan dan mengupdate proyek donasi, melanjutkan proyek lain dan mengembangkan ide dan aplikasi masjidpedia agar seluruh masjid semakin makmur.
                                </label>
                              </div>
                              <div style="margin-bottom:13px;" class="custom-control custom-checkbox">
                                <input name="infakmp" type="checkbox" class="custom-control-input" id="customCheck3">
                                <label style="color:;" class="custom-control-label" for="customCheck3">
                                  Ganti % infak ke masjidpedia (% dari jumlah total infak).
                                </label>
                            </div>
                              <div style="display:none;" id="boxpersen">
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline1">0%</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input checked type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline2">5%(Default)</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline3" name="customRadioInline1" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline3">10%</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline4" name="customRadioInline1" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline4">15%</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline5" name="customRadioInline1" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline5">20%</label>
                                </div>
                              </div>
                              <input name="rpmp" type="hidden" class="form-control form-control-sm" id="rpmp">
                              <!-- <div id="infakmasjidpedia" class="form-group">
                                <hr class="my-4">
                                <label for="rek">Jumlah Infak ke Masjidpedia</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> Rp. </span>
                                    </div>
                                    <input required name="rpmp" type="hidden" class="form-control form-control-sm" id="rpmp" placeholder="Masukkan Nominal">
                                </div>
                              </div> -->
                            </div>
                          </div>
                          <!-- <div class="col-lg-6 mx-auto mt-1 mb-5">
                            <button style="width: 100%;" type="submit" class="btn btn-dark btn-sm text-white">
                                Selanjutnya
                            </button>
                          </div> -->
                          
                        </div>
                      <!-- </form> -->
                    </div>
                </div>
                <div id="step-2" class="">
                    <!-- <h3 class="border-bottom border-gray pb-2">Step 2 Content</h3> -->
                    <div class="col-lg-12">
                      <div class="row">
                        <div class="col-lg-12">
                          <p>
                            Untuk saat ini metode pembayaran yang digunakan adalah bank transfer. Anda diwajibkan untuk mentransfer jumlah nominal yang sesuai dengan total yang tertera sampai 3(tiga) angka terakhir(tanpa pembulatan). Jangan lupa memasukkan nomor transaksi infak Anda di kolom berita sebagai referensi pada saat melakukan transfer. Terima Kasih.
                          </p>
                        </div>
                        <div class="col-lg-12">
                          <p style="font-size:1.6em;text-align:center;margin-bottom: 0px;" class="display-4">No. Transaksi Infak</p>
                          <h1 id="idtrans" style="text-align:center;margin-bottom:17px;"></h1>
                          <input type="hidden" id="idtransval" name="idtransval">
                        </div>
                        <div class="col-lg-12 mx-auto mb-4">
                          <p style="font-size:1.6em;text-align:center;margin-bottom: 0px;" class="display-4">Jumlah Transaksi</p>
                          <h1 id="nominal" style="text-align:center;padding-top: 0px;"></h1>
                          <input type="hidden" id="nominaltrans" name="nominaltrans">
                        </div>
                        <div class="col-lg-12">
                          <div id="form-step-3" role="form" data-toggle="validator">
                            <div class="form-group">
                                <label for="fullname">Dukungan (140 Karakter)</label>
                                <textarea maxlength="140" class="form-control form-control-sm" name="dukungan" rows="3"></textarea>
                                <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                            </div>
                          </div>
                        </div>
                        <div class="row container mb-3">
                          <div class="col-lg-4">
                            <!-- <div style="background: red;width:100%;height:200px;"></div> -->
                            <p style="margin-bottom: 0px;">Nama Bank</p>
                            <h3>Muamalat</h3>
                            <!-- <h5>Kode Bank : 147</h5> -->
                          </div>
                          <div class="col-lg-4">
                            <!-- <div style="background: red;width:100%;height:200px;"></div> -->
                            <p style="margin-bottom: 0px;">Nomor Rekening</p>
                            <h3>820.000.1081</h3>
                          </div>
                          <div class="col-lg-4">
                            <p style="margin-bottom: 0px;">Nama Pemilik Rekening</p>
                            <h5>Budirman (Founder Masjidpedia)</h5>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                </form>
            </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  <?php } } ?>
  <?php $this->load->view('common/footer_test_view'); ?>
  <?php $this->load->view('common/js_view'); ?>
  <?php echo js('jquery.mask.js'); ?>
  <?php echo js('toastr.min.js'); ?>
  <?php echo js('validator.min.js'); ?>
  <?php echo js('jquery.smartWizard.min.js'); ?>
  <script type="text/javascript">
        $(document).ready(function(){

            // Step show event
            $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
               //alert("You are on step "+stepNumber+" now");
               if(stepPosition === 'first'){
                   $("#prev-btn").addClass('disabled');
               }else if(stepPosition === 'final'){
                   $("#next-btn").addClass('disabled');
               }else{
                   $("#prev-btn").removeClass('disabled');
                   $("#next-btn").removeClass('disabled');
               }
            });

            // Toolbar extra buttons
            var btnFinish = $('<button></button>').text('Finish')
                                             .addClass('btn btn-info')
                                             .on('click', function(){
                                                    if( !$(this).hasClass('disabled')){
                                                        var elmForm = $("#myForm");
                                                        if(elmForm){
                                                            elmForm.validator('validate');
                                                            var elmErr = elmForm.find('.has-error');
                                                            if(elmErr && elmErr.length > 0){
                                                                alert('Oops we still have error in the form');
                                                                return false;
                                                            }else{
                                                                //alert('Great! we are ready to submit form');
                                                                elmForm.submit();
                                                                return false;
                                                            }
                                                        }
                                                    }
                                                });
            var btnCancel = $('<button></button>').text('Cancel')
                                             .addClass('btn btn-danger')
                                             .on('click', function(){
                                                    $('#smartwizard').smartWizard("reset");
                                                    $('#myForm').find("input, textarea").val("");
                                                });

            // Toolbar extra buttons
            // var btnFinish = $('<button></button>').text('Finish')
                                             // .addClass('btn btn-info')
                                             // .on('click', function(){ alert('Finish Clicked'); });
            // var btnCancel = $('<button></button>').text('Cancel')
                                             // .addClass('btn btn-danger')
                                             // .on('click', function(){ $('#smartwizard').smartWizard("reset"); });


            // Smart Wizard
            $('#smartwizard').smartWizard({
                    selected: 0,
                    theme: 'default',
                    transitionEffect:'fade',
                    showStepURLhash: true,
                    toolbarSettings: {toolbarPosition: 'bottom',
                                      toolbarButtonPosition: 'end',
                                      toolbarExtraButtons: [btnFinish, btnCancel]
                                    },
                    anchorSettings: {
                                markDoneStep: true, // add done css
                                markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                                removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                                enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                            }
            });

            $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
                //var elmForm = $("#form-step-" + stepNumber);
                //var uniq_no = Math.floor((Math.random() * 1000) + 1);
                //$("#nominal").text('Rp.'+uniq_no);
                //$("#nominal").mask('000.000.000.000.000,00', {reverse: true}).trigger('input');
                var elmForm0 = $("#form-step-0");
                var elmForm1 = $("#form-step-1");
                var elmForm2 = $("#form-step-2");
                var elmForm3 = $("#form-step-3");
                // stepDirection === 'forward' :- this condition allows to do the form validation
                // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
                if(stepDirection === 'forward' && elmForm0 || elmForm1 || elmForm2 || elmForm3){
                    elmForm0.validator('validate');
                    elmForm1.validator('validate');
                    elmForm2.validator('validate');
                    elmForm3.validator('validate');
                    var elmErr0 = elmForm0.children('.has-error');
                    if(elmErr0 && elmErr0.length > 0){
                        // Form validation failed
                        return false;
                    }
                    var elmErr1 = elmForm1.children('.has-error');
                    if(elmErr1 && elmErr1.length > 0){
                        // Form validation failed
                        return false;
                    }
                    var elmErr2 = elmForm2.children('.has-error');
                    if(elmErr2 && elmErr2.length > 0){
                        // Form validation failed
                        return false;
                    }
                    var elmErr3 = elmForm3.children('.has-error');
                    if(elmErr3 && elmErr3.length > 0){
                        // Form validation failed
                        return false;
                    }
                }
                return true;
            });

            $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
                // Enable finish button only on last step
                if(stepNumber == 2){
                    $('.btn-finish').removeClass('disabled');
                }else{
                    $('.btn-finish').addClass('disabled');
                }
            });


            // External Button Events
            $("#reset-btn").on("click", function() {
                // Reset wizard
                $('#smartwizard').smartWizard("reset");
                return true;
            });

            $("#prev-btn").on("click", function() {
                // Navigate previous
                $('#smartwizard').smartWizard("prev");
                return true;
            });

            $("#next-btn").on("click", function() {
                // Navigate next
                //
                $('#smartwizard').smartWizard("next");

                return true;
            });

            // $("#theme_selector").on("change", function() {
            //     // Change theme
            //     $('#smartwizard').smartWizard("theme", $(this).val());
            //     return true;
            // });

            // // Set selected theme on page refresh
            // $("#theme_selector").change();
        });
    </script>

  <script type="text/javascript">
    $(document).ready(function () {
          //Initialize tooltips
          $('.nav-tabs > li a[title]').tooltip();
          
          //Wizard
          $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

              var $target = $(e.target);
          
              if ($target.parent().hasClass('disabled')) {
                  return false;
              }
          });

          $(".next-step").click(function (e) {

              var $active = $('.wizard .nav-tabs li.active');
              $active.next().removeClass('disabled');
              //
              nextTab($active);

          });
          $(".prev-step").click(function (e) {

              var $active = $('.wizard .nav-tabs li.active');
              prevTab($active);

          });

          $("#customCheck1").change(function() {
              if(this.checked) {
                  //Do stuff
                  $("#infakmasjidpedia").show('slow');
              } else {
                  $("#infakmasjidpedia").hide('slow');
              }
          });
      });

      function nextTab(elem) {
          $(elem).next().find('a[data-toggle="tab"]').click();
      }
      function prevTab(elem) {
          $(elem).prev().find('a[data-toggle="tab"]').click();
      }

      //$('#rp').mask('000.000.000.000.000', {reverse: true});
      //$('#nominal').mask('000.000.000.000.000,00', {reverse: true});
  </script>
  <script>
    $("#rp").keyup(function() {
        var value = parseInt($(this).val());
        var uniq_no = Math.floor((Math.random() * 100) + 1);
        var id_trans = Math.floor(Math.random() * 899999 + 100000);

        if(!value){ value = 0; }
        
        var result = (5*value)/100;
        var total = value + uniq_no;
        var num = total.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");

        if(value == 0){$("#totalinfak").text('');
        } else {
          $("#rpmp").val(result); 
          $("#nominal").text('Rp.'+num);
          $("#nominaltrans").val(total);
          $("#idtransval").val(id_trans);  
          $("#idtrans").text('#'+id_trans);

          //$("#uniq").val(Math.floor((Math.random() * 1000) + 1));
        }
    }).keyup();

    $("#customRadioInline1").click(function(){
       var value = parseInt($("#rp").val());

      if(!value){ value = 0; }
        
      var result = (0*value);

      if(value == 0){$("#totalinfak").text('');
      } else {
        $("#rpmp").val(result); 
        $("#totalinfak").text('Rp.'+value); 
        $("#descrpmp").text('Total infak yang anda keluarkan. Terima kasih.');
      }
    });

    $("#customRadioInline2").click(function(){
       var value = parseInt($("#rp").val());

      if(!value){ value = 0; }
        
      var result = (5*value)/100;

      if(value == 0){$("#totalinfak").text('');
      } else {
        $("#rpmp").val(result); 
        $("#totalinfak").text('Rp.'+value); 
        $("#descrpmp").text('Total infak yang anda keluarkan. Terima kasih.');
      }
    });

    $("#customRadioInline3").click(function(){
       var value = parseInt($("#rp").val());

      if(!value){ value = 0; }
        
      var result = (10*value)/100;

      if(value == 0){$("#totalinfak").text('');
      } else {
        $("#rpmp").val(result); 
        $("#totalinfak").text('Rp.'+value); 
        $("#descrpmp").text('Total infak yang anda keluarkan. Terima kasih.');
      }
    });

    $("#customRadioInline4").click(function(){
       var value = parseInt($("#rp").val());

      if(!value){ value = 0; }
        
      var result = (15*value)/100;

      if(value == 0){$("#totalinfak").text('');
      } else {
        $("#rpmp").val(result); 
        $("#totalinfak").text('Rp.'+value); 
        $("#descrpmp").text('Total infak yang anda keluarkan. Terima kasih.');
      }
    });

    $("#customRadioInline5").click(function(){
       var value = parseInt($("#rp").val());

      if(!value){ value = 0; }
        
      var result = (20*value)/100;

      if(value == 0){$("#totalinfak").text('');
      } else {
        $("#rpmp").val(result); 
        $("#totalinfak").text('Rp.'+value); 
        $("#descrpmp").text('Total infak yang anda keluarkan. Terima kasih.');
      }
    });

    $("#customCheck3").change(function() {
        if(this.checked) {
            //alert('check');
            $("#boxpersen").show('slow');
        } else {
           //alert ('nay');
           $("#boxpersen").hide('slow');
        }
    });
</script>
</body>
</html>