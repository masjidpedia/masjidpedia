<!DOCTYPE html>
<html>
<head>
	<title>Masjidpedia</title>
	<?php $this->load->view('common/meta_view'); ?>
	<?php echo css('kategori.css'); ?>
	<style type="text/css">
		body { 
		  background: url(/assets/img/background-mp.jpg) no-repeat center center fixed; 
		  -webkit-background-size: cover;
		  -moz-background-size: cover;
		  -o-background-size: cover;
		  background-size: cover;
		}
	</style>
</head>
<body style=";">
	<!--<a href="#" class="back-to-top">Back to Top</a>-->
	<div class="container-fluid">
		<!-- <div class="row">
			<?php //$this->load->view('common/mainnav_view'); ?> -->
		<!-- </div> --> <!-- End Row-1 -->
		<div class="row mb-2">
			<!-- <div style="min-height:530px;background:;"> -->
				<div class="container">
					<div class="col-lg-12 mb-4">
						<div style="text-align:center;margin-top:70px;">
							<a><img style="margin:0 auto;height:100px;" class="img-responsive" alt="Responsive image" src="<?php base_url(); ?>/assets/img/mp-logo-1.png"></a>
						</div>
					</div>
					<!-- <div class="col-lg-12">
						<div style="text-align:center;">
							<button type="button" class="btn btn-outline-primary"><i class="fab fa-facebook mr-2"></i> Facebook</button>
							<button type="button" class="btn btn-outline-secondary"><i class="fab fa-twitter mr-2"></i> Twitter</button>
							<button type="button" class="btn btn-outline-warning"><i class="fab fa-instagram mr-2"></i> Instagram</button>
						</div>
					</div> -->	
				</div>
			<!-- </div> -->
		</div>

		<!-- <div style="background:#ededed;" class="row">
			<div class="col-lg-4">
				<div class="pt-5 pb-5" style="text-align:center;background:red;">
					<a href="#"><img style="margin:0 auto;height:100px;" class="img-responsive" alt="Responsive image" src="<?php base_url(); ?>/assets/img/icon-1.png"></a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="pt-5 pb-5" style="text-align:center;background:blue">
					<a href="#"><img style="margin:0 auto;height:100px;" class="img-responsive" alt="Responsive image" src="<?php base_url(); ?>/assets/img/icon-2.png"></a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="pt-5 pb-5" style="text-align:center;background:yellow">
					<a href="#"><img style="margin:0 auto;height:100px;" class="img-responsive" alt="Responsive image" src="<?php base_url(); ?>/assets/img/icon-3.png"></a>
				</div>
			</div>
			
		</div> -->

		<div class="row">
			<div class="container">
				<div class="row justify-content-md-center">
				    <!-- <div class="col col-lg-3" style="background:;text-align:center;">
				      <a href="#"><img style="margin:0 auto;height:100px;" class="img-responsive" alt="Responsive image" src="<?php base_url(); ?>/assets/img/icon-1.png"></a>
				    </div>
				    <div class="col col-lg-3" style="background:;text-align:center;">
				      <a href="#"><img style="margin:0 auto;height:100px;" class="img-responsive" alt="Responsive image" src="<?php base_url(); ?>/assets/img/icon-2.png"></a>
				    </div>
				    <div class="col col-lg-3" style="background:;text-align:center;">
				      <a href="#"><img style="margin:0 auto;height:100px;" class="img-responsive" alt="Responsive image" src="<?php base_url(); ?>/assets/img/icon-3.png"></a>
				    </div> -->
				    <div class="col-lg-12">
				    	<p style="text-align:center;" class="lead">
				    		Temukan informasi lokasi masjid, kegiatan infaq, dan sebagainya.
				    	</p>
				    </div>
				    <div class="col-lg-6 mb-4">
				    	<div class="input-group">
			              <input type="text" class="form-control form-control-lg" placeholder="Cari Masjid..">
			              <div class="input-group-append">
			                <button type="submit" class="btn btn-secondary">Search</button>
			              </div>
			            </div>
				    </div>
				    <div class="col-lg-12 text-center mb-4">
				    	<a href="<?php echo base_url(); ?>register" style="border-radius:20px;" class="btn btn-sm px-4 btn-secondary">Daftar</a>
				    	<a href="<?php echo base_url(); ?>login" style="border-radius:20px;" class="btn btn-sm px-4 btn-danger">Login</a>
				    </div>
				    <div style="text-align:center;" class="col-lg-8">
				    	<button type="button" class="btn mb-2 btn-outline-secondary btn-sm">#Terdekat</button>
						<button type="button" class="btn mb-2 btn-outline-danger btn-sm">#ProyekInfak</button>
						<button type="button" class="btn mb-2 btn-outline-secondary btn-sm">#KhatibJumat</button>
						<button type="button" class="btn mb-2 btn-outline-info btn-sm">#MasjidDalamGedung</button>
						<button type="button" class="btn mb-2 btn-outline-secondary btn-sm">#RamahAnak</button>
						<button type="button" class="btn mb-2 btn-outline-warning btn-sm">#GerakanShalatShubuhBerjamaah</button>
						<button type="button" class="btn mb-2 btn-outline-secondary btn-sm">#BukaPuasa</button>
						<button type="button" class="btn mb-2 btn-outline-secondary btn-sm">#ItikafRamadhan</button>
						<button type="button" class="btn mb-2 btn-outline-success btn-sm">#RamahTraveler</button>
						<button type="button" class="btn mb-2 btn-outline-secondary btn-sm">#InfakJamaahMandiri</button>
						<button type="button" class="btn mb-2 btn-outline-dark btn-sm">#RumahSinggahPasien</button>
				    </div>
				    
				</div>
			</div>
		</div>

		<div class="row mb-3">
			<div class="container">
				<!-- <div class="row">
					<div class="col-lg-2 mb-4">
						<div style="background:yellow;width:100%;height:100px;">
							<p style="vertical-align: middle;display: inline-block;">Infaq Masjid</p>
						</div>
					</div>
					<div class="col-lg-2 mb-4">
						<div style="background:black;width:100%;height:100px;"></div>
					</div>
					<div class="col-lg-2 mb-4">
						<div style="background:black;width:100%;height:100px;"></div>
					</div>
					<div class="col-lg-2 mb-4">
						<div style="background:black;width:100%;height:100px;"></div>
					</div>
					<div class="col-lg-2 mb-4">
						<div style="background:black;width:100%;height:100px;"></div>
					</div>
					<div class="col-lg-2 mb-4">
						<div style="background:black;width:100%;height:100px;"></div>
					</div>
					<div class="col-lg-2 mb-4">
						<div style="background:yellow;width:100%;height:100px;"></div>
					</div>
					<div class="col-lg-2 mb-4">
						<div style="background:black;width:100%;height:100px;"></div>
					</div>
					<div class="col-lg-2 mb-4">
						<div style="background:black;width:100%;height:100px;"></div>
					</div>
					<div class="col-lg-2 mb-4">
						<div style="background:blue;width:100%;height:100px;"></div>
					</div>
					<div class="col-lg-2 mb-4">
						<div style="background:black;width:100%;height:100px;"></div>
					</div>
					<div class="col-lg-2 mb-4">
						<div style="background:black;width:100%;height:100px;"></div>
					</div>
				</div> -->
			</div>
		</div>


		<section id="footer">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
          <ul class="list-unstyled list-inline social text-center">
            <li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/infomasjid/"><i class="fab fa-facebook text-primary"></i></a></li>
            <li class="list-inline-item"><a target="_blank" title="masjidpedia" href="#"><i class="fab fa-twitter text-info"></i></a></li>
            <li class="list-inline-item"><a target="_blank" title="masjidpedia_" href="https://www.instagram.com/masjidpedia_"><i class="fab fa-instagram text-warning"></i></a></li>
            <li class="list-inline-item"><a target="_blank" href="mailto:pediamasjid@gmail.com"><i class="fab fa-google-plus text-danger"></i></a></li>
            <li class="list-inline-item"><a target="_blank" href="mailto:support@masjidpedia.com" target="_blank"><i class="fa fa-envelope text-success"></i></a></li>
          </ul>
        </div>
        </hr>
      </div>  
      <!-- <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-1 mt-sm-1 mb-3 text-center text-white">
          <p class="h6 py-0">&copy All right Reversed.<a class="text-secondary ml-2" href="https://www.masjidpedia.com" target="_blank">Masjidpedia Team</a></p>
        </div>
        </hr>
      </div>  --> 
    </div>
  </section>
		
      </div>




		</div>
	</div>

	<?php
	echo js('jquery.min.js');
  	echo js('bootstrap.min.js');
  	echo js('top.js');
	?>
</body>
</html>