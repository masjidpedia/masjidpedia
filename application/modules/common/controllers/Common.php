<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common extends MX_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('mdl_common');
    }

    function make_sure_is_login()
    {
        $user_id = $this->session->userdata('user_id');
        if($user_id == '' || empty($user_id))
        {
            redirect(base_url().'login');
        }
    }

    function make_md5($password)
    {
        $new_user = md5($password);
        return $new_user;
    }

    function pword_check($username, $passwd)
    {
        $query = $this->mdl_common->pword_check($username, $passwd);
        return $query;
    }

    function get_by_table_or($table)
    {
        $query = $this->mdl_common->get_by_table_or($table);
        return $query;
    }

    function get_daftar_telepon($term)
    {
        $query = $this->mdl_common->get_daftar_telepon($term);
        return $query;
    }

    function get_one_column($table, $col, $value)
    {
        $query = $this->mdl_common->get_one_column($table, $col, $value);
        return $query;
    }

    function get_two_column($table, $col, $col1, $value, $value1)
    {
        $query = $this->mdl_common->get_two_column($table, $col, $col1, $value, $value1);
        return $query;
    }

    function get_two_column_order_by($table, $col, $col1, $value, $value1, $ordercol, $type)
    {
        $query = $this->mdl_common->get_two_column_order_by($table, $col, $col1, $value, $value1, $ordercol, $type);
        return $query;
    }

    function logout()
    {
        $this->session->set_userdata('user_id','');
        redirect('/login');
    }

    function check_table_data($table,$data)
    {
        $query = $this->mdl_common->check_table_data($table,$data);
        return $query;
    }

    function insert_to_table($table,$data)
    {
        $this->mdl_common->insert_to_table($table,$data);
    }

    function update_data_table($table,$id, $data)
    {
        $this->mdl_common->update_data_table($table,$id, $data);
    }

    function update_data_table_id($table,$col_id,$id, $data)
    {
        $this->mdl_common->update_data_table_id($table,$col_id,$id, $data);
    }

    function update($table, $where, $data)
    {
        return $this->mdl_common->update($table, $where, $data);
    }

    function delete_data_table($table,$id)
    {
        $this->mdl_common->delete_data_table($table,$id);
    }

    function get_data_table($table)
    {
        $query = $this->mdl_common->get_data_table($table);
        return $query;
    }

    function get_by_table_with_limit($table,$col,$value,$limit)
    {
        $query = $this->mdl_common->get_by_table_with_limit($table,$col,$value,$limit);
        return $query;
    }

    function total_count_data_table($table)
    {
        $query = $this->mdl_common->total_count_data_table($table);
        return $query;
    }

    function check_three_data($table,$col1,$col2,$col3,$value1,$value2,$value3)
    {
        $query = $this->mdl_common->check_three_data($table,$col1,$col2,$col3,$value1,$value2,$value3);
        return $query;
    }

    function check_two_data($table,$col1,$col2,$value1,$value2)
    {
        $query = $this->mdl_common->check_two_data($table,$col1,$col2,$value1,$value2);
        return $query;
    }

    function check_one_data($table,$col1,$value1)
    {
        $query = $this->mdl_common->check_one_data($table,$col1,$value1);
        return $query;
    }

    function check_custom_data($table,$data)
    {
        $query = $this->mdl_common->check_custom_data($table,$data);
        return $query;
    }
}
