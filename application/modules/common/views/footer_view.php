  <!-- Footer -->
  <section class="bg-light" id="footer">
    <div class="container pt-5">
      <div class="row text-lg-center text-xs-center text-sm-left text-md-left">
        <div class="col-xs-12 col-sm-4 col-md-4">
          <h5 class="text-dark">General</h5>
          <ul class="list-unstyled quick-links">
            <li><a href="#" class="text-secondary">Home</a></li>
            <li><a href="#" class="text-secondary">About</a></li>
          </ul>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
          <h5 class="text-dark">Features</h5>
          <ul class="list-unstyled quick-links">
            <li><a href="#" class="text-secondary">Home</a></li>
            <li><a href="#" class="text-secondary">About</a></li>
          </ul>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
          <h5 class="text-dark">Shortcut</h5>
          <ul class="list-unstyled quick-links">
            <li><a href="#" class="text-secondary">Login</a></li>
            <li><a href="#" class="text-secondary">Daftar jadi member</a></li>
            <li><a href="#" class="text-secondary">Undang Teman</a></li>
            <li><a href="#" class="text-secondary">Donasi</a></li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
          <ul class="list-unstyled list-inline social text-center">
            <li class="list-inline-item"><a href="#"><i class="fab fa-facebook text-primary"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fab fa-twitter text-info"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fab fa-instagram text-warning"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fab fa-google-plus text-danger"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank"><i class="fa fa-envelope text-success"></i></a></li>
          </ul>
        </div>
        </hr>
      </div>  
      <div class="row pb-3">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-1 mt-sm-1 mb-5 text-center text-dark">
          <p class="h6 py-0">Copyright &copy <?php echo date('Y'); ?>, All right Reserved.<a class="text-secondary ml-2" href="https://www.masjidpedia.com" target="_blank">Masjidpedia Team</a></p>
        </div>
        </hr>
      </div>  
    </div>
  </section>
  <!-- ./Footer -->