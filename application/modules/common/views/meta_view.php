    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <meta name="keywords" content="" /> 
    <meta name="description" content="" /> 
    <meta name="rating" content="General" /> 
    <meta name="revisit-after" content="7 days" /> 
    <meta name="classification" content="" /> 
    <meta name="distribution" content="Global" /> 
    <meta name="author" content="Masjidpedia Team" /> 
    <meta name="language" content="Indonesian" /> 
    <meta name="publisher" content="MP, Makassar, Indonesia" /> 
    <meta name="robots" content="noindex,nofollow" /> 
    <meta name="generator" content="jEdit 5.2.0" /> 
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.ico"> 
    <?php echo css('bootstrap.min.css'); ?>
    <?php echo css('general.css'); ?>
    <?php echo css('fontawesome-all.min.css'); ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --> 
    <!-- WARNING: Respond.js doesn\'t work if you view the page via file:// --> 
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->