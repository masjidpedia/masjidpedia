  <!-- Footer -->
  <section class="bg-dark" id="footer">
    <div class="container pt-5">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
          <ul class="list-unstyled list-inline social text-center">
            <li class="list-inline-item"><a href="#"><i class="fab fa-facebook text-primary"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fab fa-twitter text-info"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fab fa-instagram text-warning"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fab fa-google-plus text-danger"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank"><i class="fa fa-envelope text-success"></i></a></li>
          </ul>
        </div>
        </hr>
      </div>  
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-1 mt-sm-1 mb-3 text-center text-white">
          <p class="h6 py-0">&copy All right Reversed.<a class="text-secondary ml-2" href="https://www.masjidpedia.com" target="_blank">Masjidpedia Team</a></p>
        </div>
        </hr>
      </div>  
    </div>
  </section>
  <!-- ./Footer -->