<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_common extends CI_Model {
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

    function pword_check($username, $passwd)
    {
        $this->db->where('username', $username);
        $this->db->where('password', $passwd);
        $query = $this->db->get('tbl_admin');
        $num_rows = $query->num_rows();

        if($num_rows > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function get_one_column($table, $col, $value)
    {
        $this->db->where($col, $value);
        $query = $this->db->get($table);
        return $query;
    }

    function get_two_column($table, $col, $col1, $value, $value1)
    {
        $this->db->where($col, $value);
        $this->db->where($col1, $value1);
        $query = $this->db->get($table);
        return $query;
    }

    function get_two_column_order_by($table, $col, $col1, $value, $value1, $ordercol, $type)
    {
        $this->db->where($col, $value);
        $this->db->where($col1, $value1);
        $this->db->order_by($ordercol,$type);
        $query = $this->db->get($table);
        return $query;
    }

    function check_table_data($table,$data)
    {
        $this->db->where($data);
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function insert_to_table($table,$data)
    {
        $this->db->insert($table, $data);
    }

    function update_data_table($table,$id,$data)
    {
        $this->db->where('id', $id);
        $this->db->update($table, $data);
    }

    function update_data_table_id($table,$col_id,$id,$data)
    {
        $this->db->where($col_id, $id);
        $this->db->update($table, $data);
    }

    function update($table, $where, $data)
    {
        $this->db->update($table, $data, $where);
        return $this->db->affected_rows();
    }

    function get_data_table($table)
    {
        $query = $this->db->get($table);
        return $query;
    }

    function total_count_data_table($table)
    {
       return $this->db->count_all($table);
    }

	function get_by_table_with_limit($table,$col,$value,$limit)
	{
		$this->db->where($col, $value);
		$this->db->limit($limit);
		$query = $this->db->get($table);
        return $query;
	}

    function check_three_data($table,$col1,$col2,$col3,$value1,$value2,$value3)
    {
        $this->db->where($col1, $value1);
        $this->db->where($col2, $value2);
        $this->db->where($col3, $value3);
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function check_two_data($table,$col1,$col2,$value1,$value2)
    {
        $this->db->where($col1, $value1);
        $this->db->where($col2, $value2);
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function check_one_data($table,$col1,$value1)
    {
        $this->db->where($col1, $value1);
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function check_custom_data($table,$data)
    {
        $this->db->where($data);
        $this->db->from($table);
        return $this->db->count_all_results();
    }
}