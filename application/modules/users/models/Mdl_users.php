<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_users extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}

	function get_table()
	{
		$table = 'tbl_user';
		return $table;
	}

	function pword_check($username, $passwd)
	{
		$table = $this->get_table();
		$this->db->where('username', $username);
		$this->db->where('password', $passwd);
		$this->db->where('activated', 1);
		$query = $this->db->get($table);
		$num_rows = $query->num_rows();

		if($num_rows > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function get_where_custom($col, $value)
	{
		$table = $this->get_table();
		$this->db->where($col, $value);
		$query = $this->db->get($table);
		return $query;
	}

	function get_where_id($value)
	{
		$table = $this->get_table();
		$this->db->where('id', $value);
		$query = $this->db->get($table);
		return $query;
	}
}