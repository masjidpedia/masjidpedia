<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_users');
		$this->load->library('form_validation');
	}

	function index()
	{
		$this->load->view('login_view');
	}

	function register(){
		$this->load->view('register_view');
	}

	function register_submit(){
		$nama = $this->input->post('fullname', TRUE);
		$email = $this->input->post('email', TRUE);
		$hp = $this->input->post('hp', TRUE);
		$username = $this->input->post('username', TRUE);
		$password = $this->input->post('password', TRUE);
		$konf = $this->input->post('password2', TRUE);

		$kriteria = array (
			'username' => $username,
			'email' => $email
		);

		if($password != $konf){
			$this->session->set_flashdata('error','Konfirmasi password anda berbeda dengan password yang anda masukkan.');
			redirect('/register');
		} else {
			$cek_user = Modules::run('common/check_table_data','tbl_user',$kriteria);
			if($cek_user < 1){
				$this->load->library('email');
				$send_code = md5(((string)date('Y-m-d H:i:s')).$email);
				$data_user = array(
					'nama' => $nama,
					'username' => $username,
					'email' => $email,
					'password' => Modules::run('security/crypt_pass',$password),
					'hp' => $hp,
					'email_code' => $send_code,
					'level' => 1,
					'activated' => 0
				); 

				$message = '<!DOCTYPE html><html><head><meta charset="UTF-8"></head><body>
                <p>Kepada '.$username.',</p>
                <p>Terima kasih telah melakukan pendaftaran pada website Masjidpedia.com.</p>

                <p>Silahkan <a href="'.base_url().'register/validate/token/'.$send_code.'">klik disini</a> untuk mengaktifkan akun anda.
                Setelah akun anda aktif, anda bisa <a href="'.base_url().'login">login</a> dan mulai menggunakan fitur-fitur pada Masjidpedia.</p>
                <br/><br/><p>Terima Kasih</p>
                <br/><p>Masjidpedia Team</p>';

                //$pesan = '<a href="'.base_url().'register/validate/'.$send_code.'">Click here</a>';


                $this->email->from('info@masjidpedia.com', 'Masjidpedia');
            	$this->email->to($email);
            	$this->email->subject('Aktivasi Email');
            	$this->email->set_mailtype("html");
            	$this->email->message($message);

                if($this->email->send()){
                    Modules::run('common/insert_to_table','tbl_user',$data_user);
                    $this->session->set_flashdata('success', 'Aktifkan email anda untuk mulai menggunakan akun masjidpedia');
                    redirect('/login');
                } else {
                    $this->session->set_flashdata('error', 'Gagal tersimpan');
                    redirect('/register');
                    //echo "OK";
                }

			} else {
				$this->session->set_flashdata('error','Username atau email anda sudah ada pada database kami.');
				redirect('/register');
			}
		}
	}

	function validate_email(){
        $code = $this->uri->segment(4);
        $cek_data = Modules::run('common/check_two_data','tbl_user','email_code','activated', $code, 0);
        $data['cek_data'] = $cek_data;
        $data['emailcode'] = $code;
        $this->load->view('confirmvalidate_view', $data);
	}

	function email_validation(){
		$email_code = $this->input->post('email_code',TRUE);
        $cek_data = Modules::run('common/check_two_data','tbl_user','email_code','activated', $email_code, 0);
        if($cek_data == 1){
            $get_data = Modules::run('common/get_two_column','tbl_user','email_code','activated', $email_code, 0);
            foreach ($get_data->result() as $value) {
                # code...
                $id_user = $value->id_user;
            }

            $datauser = array(
                'email_code' => '',
                'activated' => 1
            );

            Modules::run('common/update_data_table_id','tbl_user','id_user',$id_user, $datauser);
            $this->session->set_flashdata('success', 'Aktivasi email berhasil, Silahkan login');
            $this->load->view('resultvalidate_view');
        } else {
            $this->load->view('failvalidate_view');
        }
	}

	function _in_you_go($username) //_ private
	{
		$query = $this->get_where_custom('username', $username);
		foreach ($query->result() as $row) {
			$user_id = $row->id_user;
		}

		//echo $user_id;
		$this->session->set_userdata('user_id_mp', $user_id);
		redirect('/masjid');
	}

	function submit()
	{
		$this->form_validation->set_rules('username','Username','required|max_length[30]|xss_clean');
		$this->form_validation->set_rules('password','Passwordnya','callback_passwd_checks|required|max_length[30]|xss_clean');

		if($this->form_validation->run($this) == FALSE)
		{
			$this->load->view('login_view');
		}
		else
		{
			$username = $this->input->post('username',TRUE);
			$this->_in_you_go($username);
		}

	}

	function passwd_checks($password)
	{
		$username = $this->input->post('username',TRUE);
		$pword = Modules::run('security/crypt_pass',$password);
		$result = $this->mdl_users->pword_check($username, $pword);
		if($result == FALSE)
		{
			$this->form_validation->set_message('passwd_checks','Anda memasukkan password atau username yang salah dan pastikan akun anda sudah aktif.');
			return FALSE;
		}
		else
		{
			//echo "masuk sini";
			return TRUE;
		}
	}

	function signout()
	{
		$this->session->set_userdata('user_id_mp','');
		redirect('/login');
	}

	function get($order_by)
	{
		$query = $this->mdl_users->get($order_by);
		return $query;
	}

	function get_with_limit($limit,$offset,$order_by)
	{
		$query = $this->mdl_users->get_with_limit($limit,$offset,$order_by);
		return $query;
	}

	function get_where($id)
	{
		$query = $this->mdl_users->get_where_id($id);
		return $query;
	}

	function get_where_custom($col, $value)
	{
		$query = $this->mdl_users->get_where_custom($col, $value);
		return $query;
	}

	function _insert($data)
	{
		$this->mdl_users->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_users->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_users->_delete($id);
	}

	function count_where($column, $value)
	{
		$count = $this->mdl_users->count_where($column, $value);
		return $count;
	}

	function get_max()
	{
		$max_id = $this->mdl_users->get_max();
		return $max_id;
	}

	function signup()
	{
		$this->load->view('signup_view');
	}
}
