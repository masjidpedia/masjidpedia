<!DOCTYPE html>
<html>

<head>
  <!-- Standard Meta -->
   <title>Validate Email</title>
  <?php $this->load->view('common/meta_view'); ?>
  <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico">
  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    /* .image {
      margin-top: -100px;
    } */
    .is_login {
      max-width: 450px;
    }
  </style>
  <?php $this->load->view('common/google_meta_view'); ?>
</head>

<body>
<?php //$this->load->view('common/sidebar_view'); ?>
<div class="pusher">
  <?php //$this->load->view('common/navbar_view'); ?>

  <div style="min-height:300px;margin-bottom:35px;border-bottom:0px;margin-top:150px;" id="row1" class="ui container vertical stripe segment">
    <!-- <div class="ui row_1 grid"> -->
      
    <div class="ui inverted middle aligned center aligned grid">
      <div class="column ">
        <h1 style="font-size:4em;" class="ui black image header">
          <div class="content">
            Gagal
          </div>
        </h1>
        <div class="row" style="font-size:17pt;padding:7px;">
          <p>Gagal memverifikasi email anda, periksa kembali alamat email yang anda masukkan.</p><br/>
        </div>

        <div class="row">
            <a href="<?php echo base_url(); ?>login" class="ui large orange submit button">Masuk ke JamBuka.id</a>
        </div>
      </div>
    </div>

    <!-- </div> -->
</div>
<?php //$this->load->view('common/footer_view'); ?>
</div>
</body>
</html>