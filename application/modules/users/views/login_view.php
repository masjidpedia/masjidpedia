<!DOCTYPE html>
<html>
<head>
  <title>MP | Login</title>
  <?php $this->load->view('common/meta_view'); ?>
  <?php echo css('toastr.min.css'); ?>
</head>
<body>
<?php //echo crypt('budirman', 'B4sm4l1ah') ?>
<div class="container">
  <div class="row justify-content-center mt-5">
    <aside class="col-sm-5 mt-5">
      <div class="card mt-5">
      <article class="card-body">
        <h4 class="card-title text-center mb-4 mt-1">MP | Masuk</h4>
        <hr>
        <!-- <p class="text-success text-center">Some message goes here</p> -->
        <?php echo validation_errors('<p style="font-size:.80em" class="text-danger text-center">', '</p>'); ?>
        <form method="POST" action="<?php base_url(); ?>/login/submit" role="form">
        <div class="form-group">
          <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white"> <i class="fa fa-user"></i> </span>
             </div>
            <input required name="username" class="form-control" placeholder="Username" type="text">
          </div> <!-- input-group.// -->
        </div> <!-- form-group// -->
        <div class="form-group">
          <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white"> <i class="fa fa-lock"></i> </span>
             </div>
              <input required name="password" class="form-control" placeholder="Password" type="password">

          </div> <!-- input-group.// -->
          <p style="font-size: 0.75em;" class="pt-2"><a href="#" class="">Forgot password?</a></p>
        </div> <!-- form-group// -->
        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block"> Login  </button>
        </div> <!-- form-group// -->
        
        <p class="text-center">Belum punya akun, silahkan <a href="<?php echo base_url(); ?>register" class="">klik disini.</a></p>
        </form>
      </article>
      </div> <!-- card.// -->
    </aside>
  </div>
</div>
<?php $this->load->view('common/js_view'); ?>
<?php echo js('toastr.min.js'); ?>
<?php
  if($this->session->flashdata('success')){
    ?>
    <script type="text/javascript">
        toastr.success('<?php echo $this->session->flashdata('success'); ?>', 'Masjidpedia');
    </script>
    <?php
  }
  if($this->session->flashdata('error')){
    ?>
    <script type="text/javascript">
        toastr.error('<?php echo $this->session->flashdata('error'); ?>', 'Masjidpedia');
    </script>
    <?php
  }
?>
</body>
</html>