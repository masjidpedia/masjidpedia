<!DOCTYPE html>
<html>
<head>
  <title>MP | Register</title>
  <?php $this->load->view('common/meta_view'); ?>
  <?php echo css('toastr.min.css'); ?>
</head>
<body>
<div class="container">
  <div class="row justify-content-center mt-1 mb-5">
    <aside class="col-sm-6 mt-5 mb-5">
      <div class="card mt-5 mb-5">
      <article class="card-body">
        <h4 class="card-title text-center mb-4 mt-1">MP | Daftar</h4>
        <hr>
        <?php echo validation_errors('<p style="font-size:.80em" class="text-danger text-center">', '</p>'); ?>
        <form method="POST" action="<?php base_url(); ?>/register/submit" role="form">
        <div class="form-group mb-2">
          <label style="font-size: 0.89em;margin-bottom: 3px;" for="fullname">Nama Lengkap <strong style="color:red;">*</strong></label>
          <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white"> <i class="far fa-address-card"></i> </span>
             </div>
            <input required name="fullname" class="form-control" placeholder="Masukkan Nama Lengkap" type="text">
          </div>
        </div>
        <div class="form-group mb-2">
          <label style="font-size: 0.89em;margin-bottom: 3px;" for="email">Email <strong style="color:red;">*</strong></label>
          <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white"> <i class="fas fa-envelope"></i> </span>
             </div>
            <input required name="email" class="form-control" placeholder="Masukkan Email" type="text">
          </div>
        </div>
        <div class="form-group mb-2">
          <label style="font-size: 0.89em;margin-bottom: 3px;" for="hp">Telepon <strong style="color:red;">*</strong></label>
          <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white"> <i class="fas fa-mobile"></i> </span>
             </div>
            <input required name="hp" class="form-control" placeholder="Masukkan Nomor Telepon" type="text">
          </div>
        </div>
        <div class="form-group mb-2">
          <label style="font-size: 0.89em;margin-bottom: 3px;" for="username">Username <strong style="color:red;">*</strong></label>
          <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white"> <i class="fa fa-user"></i> </span>
             </div>
            <input required name="username" class="form-control" placeholder="Masukkan Username" type="text">
          </div>
        </div>
        <div class="form-group mb-2">
          <label style="font-size: 0.89em;margin-bottom: 3px;" for="password">Password <strong style="color:red;">*</strong></label>
          <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white"> <i class="fa fa-lock"></i> </span>
             </div>
              <input required name="password" class="form-control" placeholder="Masukkan Password" type="password">
          </div>
        </div>
        <div class="form-group mb-4">
          <label style="font-size: 0.89em;margin-bottom: 3px;" for="password2">Konfirmasi Password <strong style="color:red;">*</strong></label>
          <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white"> <i class="fa fa-lock"></i> </span>
             </div>
              <input required name="password2" class="form-control" placeholder="Masukkan Konfirmasi Password" type="password">
          </div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-warning btn-block">Daftar</button>
        </div>
        <p class="text-center">Sudah punya akun, silahkan <a href="<?php echo base_url(); ?>login" class="">klik disini.</a></p>
        </form>
      </article>
      </div>
    </aside>
  </div>
</div>
<?php $this->load->view('common/js_view'); ?>
<?php echo js('toastr.min.js'); ?>
<?php
  if($this->session->flashdata('success')){
    ?>
    <script type="text/javascript">
        toastr.success('<?php echo $this->session->flashdata('success'); ?>', 'Masjidpedia');
    </script>
    <?php
  }
  if($this->session->flashdata('error')){
    ?>
    <script type="text/javascript">
        toastr.error('<?php echo $this->session->flashdata('error'); ?>', 'Masjidpedia');
    </script>
    <?php
  }
?>
</body>
</html>