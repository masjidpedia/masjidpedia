<!DOCTYPE html>
<html>

<head>
  <title>Validate Email</title>
  <?php $this->load->view('common/meta_view'); ?>
  <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico">
  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    /* .image {
      margin-top: -100px;
    } */
    .is_login {
      max-width: 450px;
    }
  </style>
  <?php //$this->load->view('common/google_meta_view'); ?>
</head>

<body>
<?php //$this->load->view('common/sidebar_view'); ?>
<div class="pusher">
  <?php //$this->load->view('common/navbar_view'); ?>

  <div style="min-height:300px;margin-bottom:35px;border-bottom:0px;margin-top:150px;" id="row1" class="ui container vertical stripe segment">
    <!-- <div class="ui row_1 grid"> -->
      
    <div class="ui inverted middle aligned center aligned grid">
      <div class="column ">
        <h1 style="font-size:3em;" class="ui black image header">
          <div class="content">
            Aktivasi Email
          </div>
        </h1>

        <?php if($cek_data == 1){ ?>
        <div class="row" style="font-size:17pt;padding:7px;">
          <p>Terima kasih telah melakukan registrasi pada website Masjidpedia, Silahkan klik tomboh dibawah untuk mengaktifkan akun masjidpedia anda.</p><br/>
        </div>

        <div class="row">
          <form id="confirmForm" method="POST" action="<?php echo base_url(); ?>email/do/validation">
            <input name="email_code" type="hidden" value="<?php echo $emailcode; ?>">
            <button id="btnSubmit" type="submit" class="ui large green submit button">Aktifkan email</button>
          </form>
        </div>
        <?php } else { ?>
          <div class="row" style="font-size:17pt;padding:7px;">
            <p>Terjadi kesalahan, akun anda mungkin sudah aktif atau pastikan token email sesuai dengan yang dikirimkan pada email anda.</p><br/>
          </div>
        <?php } ?>
      </div>
    </div>

    <!-- </div> -->
</div>
<?php //$this->load->view('common/footer_view'); ?>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/toastr.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/semantic.min.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>assets/js/jquery.simple-text-rotator.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>assets/js/general.js"></script> -->
<script>
  $(document).ready(function() {
    toastr.options = {
      "closeButton": true,
      "positionClass": "toast-bottom-right",
      "onclick": null
    };  
  });
</script>
<script>
    $(document).ready(function () {

        $("#confirmForm").submit(function (e) {

            //stop submitting the form to see the disabled button effect
            //e.preventDefault();

            //disable the submit button
            $("#btnSubmit").attr("disabled", true);

            //disable a normal button
            $("#btnTest").attr("disabled", true);

            return true;

        });
    });
</script>
  <?php
    if($this->session->flashdata('success')){
      ?>
      <script type="text/javascript">
          $(function(){
            toastr.success('<?php echo $this->session->flashdata('success'); ?>', 'SUKSES');
          });
      </script>
      <?php
    }
  ?>
</body>
</html>