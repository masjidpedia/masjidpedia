<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Masjid extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		//Modules::run('security/make_sure_is_admin');
	}

	function index1()
	{
		$data['daftarmasjid'] = Modules::run('common/get_data_table', 'tbl_mosque');
		$this->load->view('Masjid_view', $data);
	}

	function index()
	{
		$this->load->view('masjid_view');
	}

	function detail()
	{
		$this->load->view('masjiddetail_view');
	}

	function newmasjid()
	{
		Modules::run('security/make_sure_is_admin');
		$data['fasilitas'] = Modules::run('common/get_data_table', 'tbl_facility');
		$data['manajemen'] = Modules::run('common/get_data_table', 'tbl_management');
		$data['sosmed'] = Modules::run('common/get_data_table', 'tbl_sosmed');
		$this->load->view('NewMasjidStepper_view', $data);
	}

	function confirminfak()
	{
		//Modules::run('security/make_sure_is_admin');
		$this->load->view('Donatur_view');
	}

	function save()
	{
		Modules::run('security/make_sure_is_admin');
		$namamasjid = $this->input->post('masjidname', TRUE);
		$rek = $this->input->post('rek', TRUE);
		$kodepos = $this->input->post('kodepos', TRUE);
		$telepon = $this->input->post('telepon', TRUE);
		$emailmp = $this->input->post('emailmp', TRUE);
		$website = $this->input->post('website', TRUE);
		$fb = $this->input->post('fb', TRUE);
		$tw = $this->input->post('tw', TRUE);
		$desc = $this->input->post('desc', TRUE);
		$kordinat = $this->input->post('kordinat', TRUE);

		$lat_long = explode(';', $kordinat);

		// $country = $this->input->post('country', TRUE);
		// $provinsi = $this->input->post('provinsi', TRUE);
		// $kab_kota = $this->input->post('kab_kota', TRUE);
		// $kec = $this->input->post('kec', TRUE);
		// $kel = $this->input->post('kel', TRUE);
		$alamat = $this->input->post('alamat', TRUE);
		$tgl_input = date('Y-m-d');

		$kriteria = array (
			'nama_masjid' => $namamasjid,
			'rekening' => $rek,
			'email' => $emailmp,
			'telepon' => $telepon
		);

		$config_slug = array(
            'field' => 'slug',
            'table' => 'tbl_masjid'
        );
        $this->load->library('slug', $config_slug);

		$cek_masjid = Modules::run('common/check_table_data','tbl_masjid',$kriteria);
		if($cek_masjid < 1) {
			//echo "OK";
			$data_masjid = array(
				'nama_masjid' => $namamasjid,
				'slug' => $this->slug->create_uri($namamasjid),
				'rekening' => $rek,
				'email' => $emailmp,
				'telepon' => $telepon,
				'kode_pos' => $kodepos,
				'latitude' => $lat_long[0],
				'longitude' => $lat_long[1],
				'alamat' => $alamat,
				'deskripsi' => $desc,
				'web' => $website,
				'twitter' => $tw,
				'fb' => $fb,
				'tgl_input' => $tgl_input
			);
			Modules::run('common/insert_to_table','tbl_masjid',$data_masjid);
			$this->session->set_flashdata('success','Data masjid berhasil di tambahkan, Terima Kasih');
			redirect('/masjid/new');
		} else {
			//echo "NOT";
			$this->session->set_flashdata('error','Pastikan data anda masukkan dengan benar.');
			redirect('/masjid/new');
		}
	}

	function collect()
	{
		Modules::run('security/make_sure_is_admin');
		$namamasjid = $this->input->post('masjidname', TRUE);
		$rek = $this->input->post('rek', TRUE);		
		$emailmp = $this->input->post('emailmp', TRUE);
		//$kodepos = $this->input->post('kodepos', TRUE);
		$telepon = $this->input->post('telepon', TRUE);
		$kordinat = $this->input->post('kordinat', TRUE);
		$alamat = $this->input->post('alamat', TRUE);

		$lat_long = explode(';', $kordinat);
		$lat = $lat_long[0];
		$lang = $lat_long[1];

		$alquran = $this->input->post('alquran', TRUE);
		if(empty($alquran)){ $alquran = 'f'; } else { $alquran = 't'; }

		$karpetSajadah = $this->input->post('karpet', TRUE);
		if(empty($karpetSajadah)){ $karpetSajadah = 'f'; } else { $karpetSajadah = 't'; }

		$sajadah = $this->input->post('sajadah', TRUE);
		if(empty($sajadah)){ $sajadah = 'f'; } else { $sajadah = 't'; }

		$airWudhu = $this->input->post('wudhu', TRUE);
		if(empty($airWudhu)){ $airWudhu = 'f'; } else { $airWudhu = 't'; }

		$mukenah = $this->input->post('mukenah', TRUE);
		if(empty($mukenah)){ $mukenah = 'f'; } else { $mukenah = 't'; }

		$sarung = $this->input->post('sarung', TRUE);
		if(empty($sarung)){ $sarung = 'f'; } else { $sarung = 't'; }

		$penitipanBarang = $this->input->post('penitipan', TRUE);
		if(empty($penitipanBarang)){ $penitipanBarang = 'f'; } else { $penitipanBarang = 't'; }

		$loker = $this->input->post('loker', TRUE);
		if(empty($loker)){ $loker = 'f'; } else { $loker = 't'; }

		$ac = $this->input->post('ac', TRUE);
		if(empty($ac)){ $ac = 'f'; } else { $ac = 't'; }

		$kipasAngin = $this->input->post('kipas', TRUE);
		if(empty($kipasAngin)){ $kipasAngin = 'f'; } else { $kipasAngin = 't'; }

		$toilet = $this->input->post('toilet', TRUE);
		if(empty($toilet)){ $toilet = 'f'; } else { $toilet = 't'; }
		
		$ketua = $this->input->post('ketua', TRUE);
		$wakilketua = $this->input->post('wakilketua', TRUE);
		$sekertaris = $this->input->post('sekretaris', TRUE);
		$bendahara = $this->input->post('bendahara', TRUE);
		$periode = $this->input->post('periode', TRUE);
		
		$website = $this->input->post('web', TRUE);
		$facebook = $this->input->post('fb', TRUE);
		$twitter = $this->input->post('tw', TRUE);
		$youtube = $this->input->post('yt', TRUE);

		$deskripsi = $this->input->post('deskripsi', TRUE);

		$tgl_input = date('Y-m-d');
		$config_slug = array(
            'field' => 'slug',
            'table' => 'tbl_mosque'
        );
        $this->load->library('slug', $config_slug);

        $kriteria_masjid = array(
        	'namamasjid' => $namamasjid,
        	'rekening' => $rek,
        	'email' => $emailmp,
        	'telepon' => $telepon
        );

        $cek_masjid = Modules::run('common/check_table_data','tbl_mosque',$kriteria_masjid);
        if($cek_masjid < 1){
        	$master_masjid = array(
        		'namamasjid' => $namamasjid,
        		'slug' => $this->slug->create_uri($namamasjid),
        		'rekening' => $rek,
        		'email' => $emailmp,
        		'telepon' => $telepon,
        		'deskripsi' => $deskripsi,
        		'input_date' => date('Y-m-d')
        	);
        	Modules::run('common/insert_to_table','tbl_mosque',$master_masjid);
        	$id_masjid = $this->db->insert_id();

        	$manajemen_masjid = array(
        		'id_mosque' => $id_masjid,
        		'ketua' => $ketua,
        		'wakil' => $wakilketua,
        		'sekertaris' => $sekertaris,
        		'bendahara' => $bendahara,
        		'periode' => $periode
        	);

        	$lokasi_masjid = array(
        		'id_mosque' => $id_masjid,
        		'lat' => $lat,
        		'lng' => $lang,
        		'description' => $alamat,
        		'location_status' => 1
        	);

        	$sosmed_masjid = array(
        		'id_mosque' => $id_masjid,
        		'web' => $website,
        		'fb' => $facebook,
        		'twitter' => $twitter,
        		'youtube' => $youtube,
        		'gplus' => '',
        		'telegram' => '',
        		'bbm' => '',
        		'line' => '',
        		'wa' => ''
        	);

        	$fasilitas_masjid = array(
        		'id_mosque' => $id_masjid,
        		'alquran' => $alquran,
        		'sajadah' => $sajadah,
        		'karpet_sajadah' => $karpetSajadah,
        		'air_wudhu' => $airWudhu,
        		'mukenah' => $mukenah,
        		'sarung' => $sarung,
        		'penitipan_barang' => $penitipanBarang,
        		'loker_alaskaki' => $loker,
        		'ac' => $ac,
        		'kipas_angin' => $kipasAngin,
        		'toilet' => $toilet
        	);

        	Modules::run('common/insert_to_table','tbl_mosque_to_management',$manajemen_masjid);
        	Modules::run('common/insert_to_table','tbl_mosque_to_location',$lokasi_masjid);
        	Modules::run('common/insert_to_table','tbl_mosque_to_sosmed',$sosmed_masjid);
        	Modules::run('common/insert_to_table','tbl_mosque_to_facility',$fasilitas_masjid);
        	$this->session->set_flashdata('success','Data masjid berhasil di tambahkan, Terima Kasih');
			redirect('/masjid/new');
        } else {
        	$this->session->set_flashdata('error','Pastikan anda masukkan data dengan benar.');
			redirect('/masjid/new');
        }
	}

	function test()
	{
		$quran = $this->input->post('quran', TRUE);
		if(empty($quran)){
			echo "NAY";
		} else {
			echo "YAY";
		}
	}

	// function detail()
	// {
	// 	$slug = $this->uri->segment(2);
	// 	$cek_slug = Modules::run('common/check_one_data', 'tbl_mosque', 'slug', $slug);
	// 	if($cek_slug < 1){
	// 		$this->session->set_flashdata('error','Pastikan data anda masukkan dengan benar.');
	// 		redirect('/masjid');
	// 	} else {
	// 		$data['detailmasjid'] = Modules::run('common/get_one_column', 'tbl_mosque', 'slug', $slug);
	// 		foreach ($data['detailmasjid']->result() as $masjidvalue) {
	// 			$masjid_id = $masjidvalue->id;
	// 		}
	// 		$data['count_infaq'] = Modules::run('common/check_one_data', 'tbl_mosque_to_infaq', 'id_mosque', $masjid_id);
	// 		$this->load->view('DetailMasjid_view', $data);
	// 	}
	// }

	
}