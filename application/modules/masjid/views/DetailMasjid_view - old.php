<!DOCTYPE html>
<html>
<head>
  <title>Mp | Detail Masjid</title>
  <?php $this->load->view('common/meta_view'); ?>
  <?php echo css('toastr.min.css'); ?>
  <?php echo css('detailmasjid.css'); ?>
</head>
<body style="background:rgb(242, 242, 242);">
  <div class="container-fluid">
    <?php foreach ($detailmasjid->result() as $detail) { ?>
    <a class="text-dark" style="position:absolute;left:7px;top:5px;" href="<?php echo base_url(); ?>masjid"><i style="font-size:1.5em;" class="fas fa-angle-left p-3"></i></a>
    <a class="text-danger" style="position:absolute;right:7px;top:5px;" href="<?php echo base_url(); ?>logout"><i style="font-size:1.5em;" class="fas fa-power-off p-3"></i></a>
    <div class="row">
        <div class="top-bg" style="height:250px;width:100%;margin-bottom:21px;">
            <h3 style="text-align:center;padding-top:110px;" class="text-light mx-auto mt-1 mb-5"><?php echo $detail->namamasjid; ?></h3>
        </div>
    </div>


    <!-- <div class="row">
        <div style="height:300px;width:100%;background:grey;margin-bottom:21px;"></div>
    </div> -->


    <style type="text/css" id="slider-css"></style>
    <div class="spe-cor">
        <div class="container">
            <div style="margin-bottom:21px;" class="row">
                <div id="slider-2" class="carousel carousel-by-item slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="col-md-3 col-sm-4 col-4">
                                <img class="d-block img-fluid" src="<?php echo base_url(); ?>/assets/img/additional/img-1.jpg" alt="First slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-4">
                                <img class="d-block img-fluid" src="<?php echo base_url(); ?>/assets/img/additional/img-2.jpg" alt="First slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-4">
                                <img class="d-block img-fluid" src="<?php echo base_url(); ?>/assets/img/additional/img-3.jpg" alt="First slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-4">
                                <img class="d-block img-fluid" src="<?php echo base_url(); ?>/assets/img/additional/img-4.jpg" alt="Second slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-4">
                                <img class="d-block img-fluid" src="<?php echo base_url(); ?>/assets/img/additional/img-5.jpg" alt="Third slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-4">
                                <img class="d-block img-fluid" src="<?php echo base_url(); ?>/assets/img/additional/img-6.jpg" alt="Third slide">
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#slider-2" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#slider-2" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="row mb-3">
        <div class="container"><div style="width: 100%;height:70px;background-color: red;"></div></div>
    </div>
 -->



    <!-- <div class="row"> -->
        <!-- <div class="container"> -->
            <!-- <div style="margin-top:-80px;"> -->
            <!-- <div class="row" style="margin-bottom:21px;"> -->
            
                <!-- <div class="col-lg-3">
                    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/img/additional/img-1.jpg">
                </div>
                <div class="col-lg-3">
                    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/img/additional/img-2.jpg">
                </div>
                <div class="col-lg-3">
                    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/img/additional/img-3.jpg">
                </div>
                <div class="col-lg-3">
                    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/img/additional/img-4.jpg">
                </div> -->
                <!-- <div class="col-2">
                    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/img/additional/img-5.jpg">
                </div>
                <div class="col-2">
                    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/img/additional/img-6.jpg">
                </div> -->
            <!-- </div> -->
            <!-- </div> -->
        <!-- </div> -->
    <!-- </div> -->

    <div class="row">
        <div class="container">
            <div class="row">
            <div class="col-lg-4">
                <div style="width:100%;min-height:200px;background:white;padding: 5px;" class="mb-2">
                    <div class="row">
                        <div style="text-align: center;" class="col-lg-12 pt-3">
                            <span class="btn btn-lg btn-success">10/<label style="font-size:.5em;">10</label></span>
                            <p>1 Rating</p>
                        </div>
                        <div style="text-align: center;" class="col-lg-12">
                            <?php $detailfasilitas = Modules::run('common/get_one_column','tbl_mosque_to_facility','id_mosque',$detail->id); 
                            foreach ($detailfasilitas->result() as $datafasilitas) {
                            ?>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Al-quran 
                              <?php if($datafasilitas->alquran == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Air wudhu 
                              <?php if($datafasilitas->air_wudhu == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Sajadah 
                              <?php if($datafasilitas->sajadah == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Toilet 
                              <?php if($datafasilitas->toilet == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Sarung 
                              <?php if($datafasilitas->sarung == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Mukenah 
                              <?php if($datafasilitas->mukenah == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Kipas Angin 
                              <?php if($datafasilitas->kipas_angin == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              AC 
                              <?php if($datafasilitas->ac == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Penitipan 
                              <?php if($datafasilitas->penitipan_barang == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <?php } ?>
                        </div>
                        <div style="text-align: center;font-size: 2em;" class="col-lg-12">
                            <i id="smile" class="fas fa-smile"></i>
                            <i id="meh" class="fas fa-meh"></i>
                            <i id="frown" class="fas fa-frown"></i>
                        </div>
                    </div>
                </div>
                <div class="box-pieces-left mb-2">
                    <div class="row pb-3">
                        <div class="col-lg-12">
                            <div id="addressmap" style="width:100%;height:300px;background:rgba(1,1,1,0.1);"></div>
                        </div>
                        <div class="col-lg-12 mt-3 ml-3">
                            <table>
                                <tr>
                                    <?php 
                                    $detaillokasi = Modules::run('common/get_one_column','tbl_mosque_to_location','id_mosque',$detail->id); 
                                    foreach ($detaillokasi->result() as $datavalue) {
                                    ?>
                                    <td valign="top"><i class="fas fa-map-marker-alt pr-2"></i></td>
                                    <td><p class="pr-3"><?php echo $datavalue->description; ?><br/><a target="_blank" href="//maps.google.com/maps?daddr=<?php echo $datavalue->lat.','.$datavalue->lng; ?>">Get Direction</a></p></td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <td valign="top"><i class="fas fa-mobile-alt"></i></td>
                                    <td><p class="pr-3"><?php echo $detail->telepon; ?></p></td>
                                </tr>
                                <tr>
                                    <?php 
                                    $detailsosmed = Modules::run('common/get_one_column','tbl_mosque_to_sosmed','id_mosque',$detail->id); 
                                    foreach ($detailsosmed->result() as $datasosmed) {
                                    ?>
                                    <td colspan="2">
                                        <a style="color:#db4900;" href="#"><i class="fas fa-globe pr-1"></i></a>
                                        <a style="color:#1900db;" href="#"><i class="fab fa-facebook-square pr-1"></i></a>
                                        <a href="#"><i class="fab fa-twitter pr-1"></i></a>
                                        <a style="color:#db0000;" href="#"><i class="fab fa-youtube"></i></a>
                                    </td>
                                    <?php } ?>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="box-pieces-right">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                              <button class="navbar-toggler mx-auto" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                              </button>
                              <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                                <!-- <a class="navbar-brand" href="#">Hidden brand</a> -->
                                <ul class="navbar-nav mx-auto mt-2 mt-lg-0">
                                  <li class="nav-item active">
                                    <a class="nav-link pr-3 pl-3" href="#"><strong>Dashboard <span class="sr-only">(current)</span></strong></a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link pr-3 pl-3" href="#">Infaq <span class="badge badge-success">0</span></a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link pr-3 pl-3" href="#">Keuangan Masjid</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link pr-3 pl-3" href="#">Agenda Jumat</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link pr-3 pl-3" href="#">Photos <span class="badge badge-info">100</span></a>
                                  </li>
                                </ul>
                              </div>
                            </nav>
                              
                        </div>
                    </div>  
                </div>
                <!-- <div style="margin-bottom: 7px;margin-left:3px;"><span>Tinggalkan tips atau ulasan</span></div>
                 -->
                <!-- <div style="margin-bottom: 7px;margin-left:3px;color:#959595;"><span>Informasi umum</span></div> -->

                <!-- <div class="box-pieces-right-1">
                    <div class="row">
                        <div class="col-lg-12">
                            <h6 style="font-size: 1.85em;text-align: center;margin-bottom:17px;" class="display-4">INFAQ</h6>
                            <div class="row">
                                <div class="col-lg-3 col-sm-3 col-6 mb-3">
                                    <div class="card">
                                      <div style="text-align:center;" class="card-body">
                                        <p class="card-text">Pembangunan menara masjid.</p>
                                        <a href="#" class="card-link"><i class="fas fa-donate"></i></a>
                                        <a href="#" class="card-link"><i class="fas fa-share"></i></a>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-6 mb-3">
                                    <div class="card">
                                      <div style="text-align:center;" class="card-body">
                                        <p class="card-text">Pembangunan menara masjid.</p>
                                        <a href="#" class="card-link"><i class="fas fa-donate"></i></a>
                                        <a href="#" class="card-link"><i class="fas fa-share"></i></a>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-6 mb-3">
                                    <div class="card">
                                      <div style="text-align:center;" class="card-body">
                                        <p class="card-text">Pembangunan menara masjid.</p>
                                        <a href="#" class="card-link"><i class="fas fa-donate"></i></a>
                                        <a href="#" class="card-link"><i class="fas fa-share"></i></a>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-6 mb-3">
                                    <div class="card">
                                      <div style="text-align:center;" class="card-body">
                                        <p class="card-text">Pembangunan menara masjid.</p>
                                        <a href="#" class="card-link"><i class="fas fa-donate"></i></a>
                                        <a href="#" class="card-link"><i class="fas fa-share"></i></a>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div style="margin-bottom: 7px;margin-left:3px;"><span>Semua ulasan dan Tips</span><span class="badge badge-warning ml-2">0</span></div>
                <div class="box-pieces-right-1">
                    <div class="row">
                        
                    </div>
                </div>
                <div style="width:100%;min-height:217px;background:white;padding: 5px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div style="width:100%;height:200px;padding:5px;">
                                <h6 style="font-size: 2em;text-align: center;" class="display-4 mt-5 pt-3">Belum ada tips dan ulasan.</h6>
                                <p style="text-align: center;font-size: .75em;">Bantu kami meningkatkan kualitas masjid dengan memasukkan tips atau ulasan anda. Terima Kasih.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    
    <div class="row">
      <h3 style="text-align:center;" class="mx-auto mt-1 mb-3"></h3>
    </div>

    <!-- <div class="container">
        <div style="min-height:320px;" class="row mb-5 pb-5">
          
        </div> 
    </div> -->

    <?php } ?> <!-- end foreach -->
  </div>
  <?php $this->load->view('common/footer_test_view'); ?>
  <?php $this->load->view('common/js_view'); ?>
  <?php echo js('toastr.min.js'); ?>
  <?php echo js('detail.js'); ?>

    <script>
          function initMap() {
            <?php $detaillokasi = Modules::run('common/get_one_column','tbl_mosque_to_location','id_mosque',$detail->id); 
            foreach ($detaillokasi->result() as $datalokasi) {
            ?>
            var lat = {lat: <?php echo $datalokasi->lat; ?>, lng: <?php echo $datalokasi->lng; ?>}; <?php } ?>
            var map = new google.maps.Map(document.getElementById('addressmap'), {
              zoom: 16,
              center: lat
            });
            var marker = new google.maps.Marker({
              position: lat,
              map: map
            });
          }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBi98DhP4_DO7B59Rnj9wrFgWzwc8dUabc&callback=initMap">
    </script>
</body>
</html>