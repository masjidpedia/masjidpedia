<!DOCTYPE html>
<html>
<head>
  <title>Mp | Masjid</title>
  <?php $this->load->view('common/meta_view'); ?>
  <?php echo css('toastr.min.css'); ?>
  <?php echo css('masjid.css'); ?>
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <a class="text-danger" style="position:absolute;right:7px;top:5px;" href="<?php echo base_url(); ?>logout"><i style="font-size:1.5em;" class="fas fa-power-off p-3"></i></a>
      <h3 class="display-4 mx-auto mt-5 mb-5">Daftar Donatur</h3>
    </div>
    <!-- <div class="row">
      <div class="mx-auto"><a href="<?php echo base_url(); ?>masjid/new" class="text-success"><i style="font-size:4em" class="fab fa-ussunnah"></i></a></div>
    </div> -->

     <div class="container">
        <div style="min-height:300px;" class="row mb-5 pb-5">
          <?php // foreach($daftarmasjid->result() as $masjid){ ?>
            <!-- <div class="col-sm-6 col-lg-3 col-6 mt-4">
                <div class="card">
                    <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/additional/mp-noimg.jpg">
                    <div class="card-block">
                        <figure class="profile">
                            <img src="<?php echo base_url(); ?>assets/img/additional/mp-noimg-1.jpg" class="profile-avatar" alt="">
                        </figure>
                        <h4 style="height:50px;" class="card-title mt-3"><a class="text-dark" href="#"><?php echo $masjid->namamasjid; ?></a></h4>
                        <div class="card-text">
                            <?php
                              if (strlen($masjid->deskripsi) > 150)
                                $cutdesc = substr($masjid->deskripsi, 0, 90) . ' ...';
                              else
                                $cutdesc = $masjid->deskripsi;

                              echo $cutdesc;
                            ?>
                        </div>
                    </div>
                    <div class="card-footer">
                        <small><?php echo $masjid->input_date; ?></small>
                        <a href="<?php echo base_url(); ?>masjid/<?php echo $masjid->slug; ?>"><button class="btn btn-danger float-right btn-sm">Detail</button></a> 
                    </div>
                </div>
            </div> -->
            <?php // } ?>
        </div>
        
  </div>
  </div>
  <?php $this->load->view('common/footer_test_view'); ?>
  <?php $this->load->view('common/js_view'); ?>
  <?php echo js('toastr.min.js'); ?>
</body>
</html>