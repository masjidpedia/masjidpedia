<!DOCTYPE html>
<html>
<head>
  <title>Mp | Detail Masjid</title>
  <?php $this->load->view('common/meta_view'); ?>
  <?php echo css('toastr.min.css'); ?>
  <?php echo css('detailmasjid.css'); ?>
  <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&autoLogAppEvents=1&version=v3.1&appId=1208287575862428';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</head>
<body style="background:rgb(242, 242, 242);">
  <div class="container-fluid">
    <?php foreach ($detailmasjid->result() as $detail) { ?>
    <a class="text-dark" style="position:absolute;left:7px;top:5px;" href="<?php echo base_url(); ?>masjid"><i style="font-size:1.5em;" class="fas fa-angle-left p-3"></i></a>
    <a class="text-danger" style="position:absolute;right:7px;top:5px;" href="<?php echo base_url(); ?>logout"><i style="font-size:1.5em;" class="fas fa-power-off p-3"></i></a>
    <div class="row">
        <div class="top-bg" style="height:250px;width:100%;margin-bottom:21px;">
            <h3 style="text-align:center;padding-top:110px;" class="text-light mx-auto mt-1 mb-5"><?php echo $detail->namamasjid; ?></h3>
        </div>
    </div>


    <!-- <div class="row">
        <div style="height:300px;width:100%;background:grey;margin-bottom:21px;"></div>
    </div> -->


    <!-- <style type="text/css" id="slider-css"></style>
    <div class="spe-cor">
        <div class="container">
            <div style="margin-bottom:21px;" class="row">
                <div id="slider-2" class="carousel carousel-by-item slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="col-md-3 col-sm-4 col-4">
                                <img class="d-block img-fluid" src="<?php echo base_url(); ?>/assets/img/additional/img-1.jpg" alt="First slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-4">
                                <img class="d-block img-fluid" src="<?php echo base_url(); ?>/assets/img/additional/img-2.jpg" alt="First slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-4">
                                <img class="d-block img-fluid" src="<?php echo base_url(); ?>/assets/img/additional/img-3.jpg" alt="First slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-4">
                                <img class="d-block img-fluid" src="<?php echo base_url(); ?>/assets/img/additional/img-4.jpg" alt="Second slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-4">
                                <img class="d-block img-fluid" src="<?php echo base_url(); ?>/assets/img/additional/img-5.jpg" alt="Third slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-4">
                                <img class="d-block img-fluid" src="<?php echo base_url(); ?>/assets/img/additional/img-6.jpg" alt="Third slide">
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#slider-2" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#slider-2" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div> -->

    <!-- <div class="row mb-3">
        <div class="container"><div style="width: 100%;height:70px;background-color: red;"></div></div>
    </div>
 -->



    <!-- <div class="row"> -->
        <!-- <div class="container"> -->
            <!-- <div style="margin-top:-80px;"> -->
            <!-- <div class="row" style="margin-bottom:21px;"> -->
            
                <!-- <div class="col-lg-3">
                    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/img/additional/img-1.jpg">
                </div>
                <div class="col-lg-3">
                    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/img/additional/img-2.jpg">
                </div>
                <div class="col-lg-3">
                    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/img/additional/img-3.jpg">
                </div>
                <div class="col-lg-3">
                    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/img/additional/img-4.jpg">
                </div> -->
                <!-- <div class="col-2">
                    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/img/additional/img-5.jpg">
                </div>
                <div class="col-2">
                    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/img/additional/img-6.jpg">
                </div> -->
            <!-- </div> -->
            <!-- </div> -->
        <!-- </div> -->
    <!-- </div> -->

    <div class="row">
        <div class="container">
            <div class="row">
            <div class="col-lg-4">
                <div style="width:100%;min-height:200px;background:white;padding: 5px;" class="mb-2">
                    <div class="row">
                        <div style="text-align: center;" class="col-lg-12 pt-3">
                            <span class="btn btn-lg btn-success">10/<label style="font-size:.5em;">10</label></span>
                            <p>1 Rating</p>
                        </div>
                        <div style="text-align: center;" class="col-lg-12">
                            <?php $detailfasilitas = Modules::run('common/get_one_column','tbl_mosque_to_facility','id_mosque',$detail->id); 
                            foreach ($detailfasilitas->result() as $datafasilitas) {
                            ?>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Al-quran 
                              <?php if($datafasilitas->alquran == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Air wudhu 
                              <?php if($datafasilitas->air_wudhu == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Sajadah 
                              <?php if($datafasilitas->sajadah == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Toilet 
                              <?php if($datafasilitas->toilet == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Sarung 
                              <?php if($datafasilitas->sarung == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Mukenah 
                              <?php if($datafasilitas->mukenah == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Kipas Angin 
                              <?php if($datafasilitas->kipas_angin == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              AC 
                              <?php if($datafasilitas->ac == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-secondary mb-2">
                              Penitipan 
                              <?php if($datafasilitas->penitipan_barang == 't'){ ?><span class="badge badge-success"><i class="fas fa-check"></i><?php } else { ?> <span class="badge badge-danger"><i class="fas fa-times"></i></span> <?php } ?>
                            </button>
                            <?php } ?>
                        </div>
                        <div style="text-align: center;font-size: 2em;" class="col-lg-12">
                            <?php 
                                $cek_rating = Modules::run('common/check_one_data', 'tbl_mosque_to_value', 'id_mosque', $detail->id);
                                if($cek_rating < 1){ ?>
                                    <i id="smile" class="fas fa-smile"></i>
                                    <i id="meh" class="fas fa-meh"></i>
                                    <i id="frown" class="fas fa-frown"></i>
                            <?php
                                } else {
                                    $detailrating = Modules::run('common/get_one_column','tbl_mosque_to_value','id_mosque',$detail->id); 
                                    foreach ($detailrating->result() as $datarating) { ?>
                                        <?php if($datarating->nilai == 1){ ?>
                                            <i style="color:#00b709;" id="smile" class="fas fa-smile"></i>
                                            <i id="meh" class="fas fa-meh"></i>
                                            <i id="frown" class="fas fa-frown"></i>

                                        <?php } else if($datarating->nilai == 2){ ?>
                                            <i id="smile" class="fas fa-smile"></i>
                                            <i style="color:#fff321;" id="meh" class="fas fa-meh"></i>
                                            <i id="frown" class="fas fa-frown"></i>
                                        <?php } else if($datarating->nilai == 3){ ?>
                                            <i id="smile" class="fas fa-smile"></i>
                                            <i id="meh" class="fas fa-meh"></i>
                                            <i style="color:#ff3838;" id="frown" class="fas fa-frown"></i>
                            <?php
                                        }
                            
                                    }
                                }
                            ?>
                        </div>
                        <div class="col-lg-12">
                            <small class="form-text text-center text-muted mb-2">
                                Your Rating : 
                                <?php 
                                    $cek_rating = Modules::run('common/check_one_data', 'tbl_mosque_to_value', 'id_mosque', $detail->id);
                                    if($cek_rating < 1){ ?>
                                    Belum ada
                                    <?php } else {
                                        $detailrating = Modules::run('common/get_one_column','tbl_mosque_to_value','id_mosque',$detail->id); 
                                        foreach ($detailrating->result() as $datarating) {
                                            if($datarating->nilai == 1){ echo "Good"; }
                                            else if ($datarating->nilai == 2) { echo "Standard"; }
                                            else if ($datarating->nilai == 3) { echo "Bad"; }
                                        } 
                                    } 
                                ?>
                            </small>
                        </div> 
                        <div class="col-lg-12">
                            <?php
                                //echo uniqid();
                                ?>
                        </div>           
                    </div>
                </div>
                <div class="box-pieces-left mb-2">
                    <div class="row pb-3">
                        <div class="col-lg-12">
                            <div id="addressmap" style="width:100%;height:300px;background:rgba(1,1,1,0.1);"></div>
                        </div>
                        <div class="col-lg-12 mt-3 ml-3">
                            <table>
                                <tr>
                                    <?php 
                                    $detaillokasi = Modules::run('common/get_one_column','tbl_mosque_to_location','id_mosque',$detail->id); 
                                    foreach ($detaillokasi->result() as $datavalue) {
                                    ?>
                                    <td valign="top"><i class="fas fa-map-marker-alt pr-2"></i></td>
                                    <td><p class="pr-3"><?php echo $datavalue->description; ?><br/><a target="_blank" href="//maps.google.com/maps?daddr=<?php echo $datavalue->lat.','.$datavalue->lng; ?>">Get Direction</a></p></td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <td valign="top"><i class="fas fa-mobile-alt"></i></td>
                                    <td><p class="pr-3"><?php echo $detail->telepon; ?></p></td>
                                </tr>
                                <tr>
                                    <?php 
                                    $detailsosmed = Modules::run('common/get_one_column','tbl_mosque_to_sosmed','id_mosque',$detail->id); 
                                    foreach ($detailsosmed->result() as $datasosmed) {
                                    ?>
                                    <td colspan="2">
                                        <a style="color:#db4900;" href="#"><i class="fas fa-globe pr-1"></i></a>
                                        <a style="color:#1900db;" href="#"><i class="fab fa-facebook-square pr-1"></i></a>
                                        <a href="#"><i class="fab fa-twitter pr-1"></i></a>
                                        <a style="color:#db0000;" href="#"><i class="fab fa-youtube"></i></a>
                                    </td>
                                    <?php } ?>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="box-pieces-right">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                              <button class="navbar-toggler mx-auto" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                              </button>
                              <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                                <!-- <a class="navbar-brand" href="#">Hidden brand</a> -->
                                <ul class="nav navbar-nav mx-auto mt-2 mt-lg-0" id="myTab" role="tablist">
                                  <li class="nav-item">
                                    <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active pr-3 pl-3" href="#">Dashboard <span class="sr-only">(current)</span></a>
                                  </li>
                                  <li class="nav-item">
                                    <a id="infaq-tab" data-toggle="tab" href="#infaq" role="tab" aria-controls="infaq" aria-selected="false" class="nav-link pr-3 pl-3" href="#">Infaq <span class="badge badge-success"><?php echo $count_infaq; ?></span></a>
                                  </li>
                                  <li class="nav-item">
                                    <a id="finansial-tab" data-toggle="tab" href="#finansial" role="tab" aria-controls="finansial" aria-selected="false" class="nav-link pr-3 pl-3" href="#">Keuangan Masjid</a>
                                  </li>
                                  <li class="nav-item">
                                    <a id="jumat-tab" data-toggle="tab" href="#jumat" role="tab" aria-controls="jumat" aria-selected="false" class="nav-link pr-3 pl-3" href="#">Agenda Jumat</a>
                                  </li>
                                  <li class="nav-item">
                                    <a id="photos-tab" data-toggle="tab" href="#photos" role="tab" aria-controls="photos" aria-selected="false" class="nav-link pr-3 pl-3" href="#">Photos <span class="badge badge-info">0</span></a>
                                  </li>
                                </ul>
                              </div>
                            </nav>
                              
                        </div>
                    </div>  
                </div>

            <div class="tab-content" id="myTabContent">
                
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                <div style="margin-bottom: 7px;margin-left:3px;"><span>Semua ulasan dan Tips</span><span class="badge badge-warning ml-2">0</span></div>
                <?php
                $user_id = $this->session->userdata('user_id_mp');
                $cek_username = Modules::run('common/check_one_data','tbl_user','id_user',$user_id);
                if($cek_username > 0){
                ?> 
                <div class="box-pieces-right-1">
                    <form>
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="form-group">
                                <textarea required rows="5" id="deskripsi" name="deskripsi" placeholder="Masukkan ulasan atau tips anda." class="form-control"></textarea>
                            </div>
                        </div> 
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div id="update_media" class="form-group">
                                      <span title="Upload gambar" class="btn btn-outline-secondary btn-sm"><i class="far fa-image mr-2"></i> Unggah gambar</span>
                                      <!-- <span title="Share di facebook" class="btn btn-info btn-sm ml-1"><i class="fab fa-facebook-square"></i></span>
                                      <span title="Share di twitter" class="btn btn-success btn-sm ml-1"><i class="fab fa-twitter"></i></span> -->
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div id="update_btn" class="form-group">
                                      <button type="submit" class="btn btn-warning btn-sm text-white">
                                          Simpan tips | ulasan anda
                                      </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <?php } ?>
                <div style="width:100%;min-height:217px;background:white;padding: 5px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div style="width:100%;height:200px;padding:5px;margin-bottom:50px;">
                                <h6 style="font-size: 2em;text-align: center;" class="display-4 mt-5 pt-3">Belum ada tips dan ulasan.</h6>
                                <p style="text-align: center;font-size: .75em;">Bantu kami meningkatkan kualitas masjid dengan memasukkan tips atau ulasan anda. Terima Kasih.</p>
                            </div>
                        </div>
                    </div>
                </div>

                </div> <!-- end tab dashboard -->

                <div class="tab-pane fade" id="infaq" role="tabpanel" aria-labelledby="infaq-tab">
                    <div style="margin-bottom: 7px;margin-left:3px;">
                        <span>Semua Infaq</span><span class="badge badge-warning ml-2"><?php echo $count_infaq; ?></span>
                        <!-- <button type="button" class="btn btn-danger float-right">Danger</button> -->
                    </div>
                    <div style="text-align: center;" class="box-pieces-right-new-btn">
                        <button style="color:white;" type="button" class="btn btn-warning btn-sm">Tambah Proyek Infaq</button>
                    </div>
                    <?php if($count_infaq == 0){ ?>
                        <div style="width:100%;min-height:217px;background:white;padding: 5px;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div style="width:100%;height:200px;padding:5px;margin-bottom:50px;text-align: center;">
                                        <h6 style="font-size: 2em;text-align: center;" class="display-4 mt-5 pt-3">Belum ada Infaq.</h6>
                                        <p style="text-align: center;font-size: .75em;">Bantu kami meningkatkan kualitas masjid dengan membantu memberikan infaq terbaik anda. Terima Kasih.</p>
                                        <button type="button" class="btn btn-outline-warning btn-sm">Tambah Proyek Infaq</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                    <div class="box-pieces-right">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="list-group">
                                <?php
                                $datainfaq = Modules::run('common/get_one_column', 'tbl_mosque_to_infaq', 'id_mosque', $detail->id);
                                foreach ($datainfaq->result() as $infaqvalue) { ?>
                                    <div style="margin-bottom:13px;" class="list-group-item list-group-item-action flex-column align-items-start">
                                        <div class="d-flex w-100 justify-content-between">
                                          <a href="<?php echo base_url(); ?>masjid/<?php echo $detail->slug; ?>/infaq/<?php echo $infaqvalue->infaq_id; ?>"><h5 class="mb-1"><?php echo $infaqvalue->judul; ?></h5></a>
                                          <small>3 days ago</small>
                                        </div>
                                        <div>
                                            <a href="#" class="btn btn-dark btn-sm mb-1">
                                                Butuh <span class="badge badge-light">Rp.150.000.000,-</span>
                                            </a>
                                            <a href="#" class="btn btn-outline-success btn-sm mb-1">
                                                Terkumpul <span class="badge badge-light">0,1 %</span>
                                            </a>
                                        </div>
                                        <p class="mb-1">Deskripsi belum ada.</p>
                                        <small>
                                            <a href="#" class="btn btn-sm btn-success mb-1 disabled">
                                                Opened
                                            </a>
                                            <a href="#" class="btn disabled btn-sm btn-outline-primary mb-1">
                                                <i class="fab fa-facebook-f mr-2"></i> Share | <span class="badge badge-light">0</span>
                                            </a>
                                            <a href="#" class="btn disabled btn-sm btn-outline-secondary mb-1">
                                                <i class="fas fa-comments mr-2"></i> Comment | <span class="badge badge-light">0</span>
                                            </a>
                                            <a href="#" class="btn disabled btn-sm btn-outline-info mb-1">
                                                <i class="fas fa-donate mr-2"></i> Donasi
                                            </a>
                                        </small>
                                      </div>

                                
                                <?php } ?>
                                </div>

                            </div> 
                        </div>
                        
                    </div>

                    <?php } ?>
                    <!-- <div style="width:100%;min-height:217px;background:white;padding: 5px;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="width:100%;height:200px;padding:5px;margin-bottom:50px;text-align: center;">
                                    <h6 style="font-size: 2em;text-align: center;" class="display-4 mt-5 pt-3">Belum ada Infaq.</h6>
                                    <p style="text-align: center;font-size: .75em;">Bantu kami meningkatkan kualitas masjid dengan membantu memberikan infaq terbaik anda. Terima Kasih.</p>
                                    <button type="button" class="btn btn-outline-warning btn-sm">Tambah Proyek Infaq</button>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div> <!-- end tab infaq -->

                <div class="tab-pane fade" id="finansial" role="tabpanel" aria-labelledby="finansial-tab">
                    <div style="margin-bottom: 7px;margin-left:3px;"><span>Semua Keuangan Masjid</span><span class="badge badge-warning ml-2">0</span></div>

                    <div style="text-align: center;" class="box-pieces-right-new-btn">
                        <button style="color:white;" type="button" class="btn btn-danger btn-sm">Tambah Data Keuangan</button>
                    </div>

                    <!-- <div class="box-pieces-right">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="jumbotron jumbotron-fluid">
                                  <div class="container">
                                    <button type="button" class="btn btn-primary btn-sm">Saldo Kas</button>
                                    <h1 class="display-4">Rp. 0,-</h1>
                                    <p class="lead">Untuk melihat detail <span class="badge badge-info">Pemasukan</span> dan <span class="badge badge-warning">Pengeluaran</span> keuangan masjid, silahkan scroll kebawah.</p>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-pieces-right">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="list-group">
                                  <div style="margin-bottom:13px;" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <span class="badge badge-info">Pemasukan</span>
                                    <div class="d-flex w-100 justify-content-between">
                                      <h5 class="mb-1">Kotak amal jumat</h5>
                                      <small>12 Mei 2018</small>
                                    </div>
                                    <div>
                                        <a href="#" class="btn btn-light btn-sm mb-1">
                                            Dana <span class="badge badge-light">Rp.500.000,-</span>
                                        </a>
                                    </div>
                                  </div>

                                  <div style="margin-bottom:13px;" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <span class="badge badge-info">Pemasukan</span>
                                    <div class="d-flex w-100 justify-content-between">
                                      <h5 class="mb-1">Sumbangan dari kepala camat biringkanaya</h5>
                                      <small>21 Mei 2018</small>
                                    </div>
                                    <div>
                                        <a href="#" class="btn btn-light btn-sm mb-1">
                                            Dana <span class="badge badge-light">Rp.1.500.000,-</span>
                                        </a>
                                    </div>
                                  </div>

                                  <div style="margin-bottom:13px;" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <span class="badge badge-warning">Pengeluaran</span>
                                    <div class="d-flex w-100 justify-content-between">
                                      <h5 class="mb-1">Pembelian lampu tempat wudhu dan kamar mandi.</h5>
                                      <small>22 Mei 2018</small>
                                    </div>
                                    <div>
                                        <a href="#" class="btn btn-light btn-sm mb-1">
                                            Dana <span class="badge badge-light">Rp.430.000,-</span>
                                        </a>
                                    </div>
                                  </div>
                                </div> 
                            </div>
                        </div>
                    </div> -->

                    <div style="width:100%;min-height:217px;background:white;padding: 5px;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="width:100%;height:200px;padding:5px;margin-bottom:50px;text-align: center;">
                                    <h6 style="font-size: 2em;text-align: center;" class="display-4 mt-5 pt-3">Belum ada informasi.</h6>
                                    <p style="text-align: center;font-size: .75em;">Anda dapat melihat keuangan masjid secara transparan. Terima Kasih.</p>
                                    <button type="button" class="btn btn-outline-danger btn-sm">Tambah Data Keuangan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end tab finansial -->

                <div class="tab-pane fade" id="jumat" role="tabpanel" aria-labelledby="jumat-tab">
                    <div style="margin-bottom: 7px;margin-left:3px;"><span>Semua Kegiatan Jumat</span><span class="badge badge-warning ml-2">0</span></div>

                    <div style="text-align: center;" class="box-pieces-right-new-btn">
                        <button style="color:white;" type="button" class="btn btn-success btn-sm">Tambah Agenda Jumat</button>
                    </div>

                    <div style="width:100%;min-height:217px;background:white;padding: 5px;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="width:100%;height:200px;padding:5px;margin-bottom:50px;text-align: center;">
                                    <h6 style="font-size: 2em;text-align: center;" class="display-4 mt-5 pt-3">Belum ada kegiatan.</h6>
                                    <p style="text-align: center;font-size: .75em;">Anda dapat kegiatan setiap jumat pada suatu masjid. Terima Kasih.</p>
                                    <button type="button" class="btn btn-outline-success btn-sm">Tambah Kegiatan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end tab jumat -->

                <div class="tab-pane fade" id="photos" role="tabpanel" aria-labelledby="photos-tab">
                    <div style="margin-bottom: 7px;margin-left:3px;"><span>Semua Photo</span><span class="badge badge-warning ml-2">0</span></div>

                    <div style="text-align: center;" class="box-pieces-right-new-btn">
                        <button style="color:white;" type="button" class="btn btn-primary btn-sm">Tambah Photo</button>
                    </div>

                    <div style="width:100%;min-height:217px;background:white;padding: 5px;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="width:100%;height:200px;padding:5px;margin-bottom:50px;text-align: center;">
                                    <h6 style="font-size: 2em;text-align: center;" class="display-4 mt-5 pt-3">Belum ada Photo.</h6>
                                    <p style="text-align: center;font-size: .75em;">Photo membantu anda mengetahui kondisi dan kegiatan yang diadakan oleh masjid. Terima Kasih.</p>
                                    <button type="button" class="btn btn-outline-primary btn-sm">Tambah Photo</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end tab photos -->

            </div>


            </div>
            </div>
        </div>
    </div>
    
    <div class="row">
      <h3 style="text-align:center;" class="mx-auto mt-1 mb-3"></h3>
    </div>

    <!-- <div class="container">
        <div style="min-height:320px;" class="row mb-5 pb-5">
          
        </div> 
    </div> -->

    <?php } ?> <!-- end foreach -->
  </div>
  <?php $this->load->view('common/footer_test_view'); ?>
  <?php $this->load->view('common/js_view'); ?>
  <?php echo js('toastr.min.js'); ?>
  <?php
      if($this->session->flashdata('success')){
        ?>
        <script type="text/javascript">
            toastr.success('<?php echo $this->session->flashdata('success'); ?>', 'Masjidpedia');
        </script>
        <?php
      }
      if($this->session->flashdata('error')){
        ?>
        <script type="text/javascript">
            toastr.error('<?php echo $this->session->flashdata('error'); ?>', 'Masjidpedia');
        </script>
        <?php
      }
    ?>
  <?php echo js('detail.js'); ?>

    <script>
          function initMap() {
            <?php $detaillokasi = Modules::run('common/get_one_column','tbl_mosque_to_location','id_mosque',$detail->id); 
            foreach ($detaillokasi->result() as $datalokasi) {
            ?>
            var lat = {lat: <?php echo $datalokasi->lat; ?>, lng: <?php echo $datalokasi->lng; ?>}; <?php } ?>
            var map = new google.maps.Map(document.getElementById('addressmap'), {
              zoom: 16,
              center: lat
            });
            var marker = new google.maps.Marker({
              position: lat,
              map: map
            });
          }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBi98DhP4_DO7B59Rnj9wrFgWzwc8dUabc&callback=initMap">
    </script>
    <script type="text/javascript">
        var anchor = window.location.hash;
        $('a[href="${anchor}"]').tab('show');
    </script>
</body>
</html>