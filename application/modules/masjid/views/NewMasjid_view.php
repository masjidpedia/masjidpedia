<!DOCTYPE html>
<html>
<head>
  <title>Mp | Tambah Masjid</title>
  <?php $this->load->view('common/meta_view'); ?>
  <?php echo css('toastr.min.css'); ?>
  <?php echo css('masjid.css'); ?>
  <style type="text/css">
      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        /* margin-left: 12px; */
         padding: 0 11px 0 13px; 
        text-overflow: ellipsis;
        margin-bottom: 5px;
        width: 100%;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }
  </style>
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <a class="text-danger" style="position:absolute;right:7px;top:5px;" href="<?php echo base_url(); ?>logout"><i style="font-size:1.5em;" class="fas fa-power-off p-3"></i></a>
      <h3 style="text-align: center;" class="display-4 mx-auto mt-5 mb-5">Tambah Data MP</h3>
    </div>
    <div class="row">
      <div class="mx-auto"><a href="<?php echo base_url(); ?>masjid" class="text-danger"><i style="font-size:3em" class="fas fa-chevron-circle-left"></i></a></div>
    </div>

     <div class="container">
        <div class="row mb-5 pb-5 mt-4">
            <div class="col-lg-12">
                <form action="<?php echo base_url(); ?>/masjid/save" method="POST" role="form">
                    <div class="row">
                        <div class="col-lg-6">
                            <p class="h6 display-4 text-secondary">Basic</p>
                            <div class="form-group">
                                <label for="masjidname">Nama Masjid <strong style="color:red;">*</strong></label>
                                <input required name="masjidname" required type="text" class="form-control form-control-lg" id="masjidname" placeholder="Masukkan nama masjid">
                            </div>
                            <div class="form-group">
                                <label for="rek">Rekening</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="far fa-credit-card"></i> </span>
                                    </div>
                                    <input name="rek" type="text" class="form-control form-control-lg" id="rek" placeholder="Masukkan Rekening Masjid">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kodepos">Kodepos</label>
                                <input name="kodepos" type="text" class="form-control form-control-lg" id="kodepos" placeholder="Masukkan Kodepos">
                            </div>
                            <div class="form-group">
                                <label for="telepon">Telepon</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fas fa-phone"></i> </span>
                                    </div>
                                    <input name="telepon" type="text" class="form-control form-control-lg" id="telepon" placeholder="Masukkan Telepon">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fas fa-envelope"></i> </span>
                                    </div>
                                    <input name="emailmp" type="email" class="form-control form-control-lg" id="email" placeholder="Masukkan Alamat Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="website">Website</label>
                                <input name="website" type="text" class="form-control form-control-lg" id="website" placeholder="Masukkan Alamat Website">
                            </div>
                            <div class="form-group">
                                <label for="fb">Facebook</label>
                                <input name="fb" type="text" class="form-control form-control-lg" id="fb" placeholder="Masukkan Alamat Facebook masjid">
                            </div>
                            <div class="form-group">
                                <label for="tw">Twitter</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fas fa-at"></i> </span>
                                    </div>
                                    <input name="tw" type="text" class="form-control form-control-lg" id="tw" placeholder="Masukkan Alamat Twitter masjid">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="desc">Deskripsi</label>
                                <textarea rows="5" name="desc" placeholder="Masukkan Deskripsi Masjid" class="form-control form-control-lg"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <p class="h6 display-4 text-secondary">Location</p>
                            <!-- <div class="form-group">
                                <label for="country">Negara</label>
                                <select name="country" class="form-control form-control-lg" id="country">
                                  <option>Pilih negara</option>
                                  <option>Indonesia</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="provinsi">Provinsi</label>
                                <select name="provinsi" class="form-control form-control-lg" id="provinsi">
                                  <option>Pilih provinsi</option>
                                  <option>Indonesia</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="kab_kota">Kabupaten/Kota</label>
                                <select name="kab_kota" class="form-control form-control-lg" id="kab_kota">
                                  <option>Pilih kabupaten/kota</option>
                                  <option>Indonesia</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="kec">Kecamatan</label>
                                <select name="kec" class="form-control form-control-lg" id="kec">
                                  <option>Pilih kecamatan</option>
                                  <option>Indonesia</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="kel">Kelurahan</label>
                                <select name="kel" class="form-control form-control-lg" id="kel">
                                  <option>Pilih kelurahan</option>
                                  <option>Indonesia</option>
                                </select>
                            </div> -->
                            <div class="form-group">
                                <label for="address">Pin Lokasi</label>
                                <div class="alert alert-success" role="alert">
                                  Pindahkan (drag) pin marker sesuai dengan alamat masjid anda.
                                </div>
                                <div style="height:610px;border:solid 1px grey;" id="map"></div>
                                <small style="margin-top:3px;" class="form-text text-muted">Pindahkan pin marker sesuai dengan alamat masjid anda.</small>
                                <div id="infoPanel">
                                    <!-- <b>Marker status:</b> -->
                                    <!-- <div style="display:none;" id="markerStatus"><i>Click and drag the marker.</i></div> -->
                                    <!-- <b>Koordinat lokasi:</b> -->
                                    <!-- <div id="info"></div> -->
                                    <input id="info" type="hidden" name="kordinat">
                                    <!-- <b>Closest matching address:</b> -->
                                    <!-- <div id="address"></div> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address">Alamat <strong style="color:red;">*</strong></label>
                                <textarea required id="address" name="alamat" placeholder="Masukkan Alamat Masjid" class="form-control form-control-lg"></textarea>
                                <small style="margin-top:3px;" class="form-text text-muted">Jika alamat anda tidak sesuai, silahkan ganti dengan alamat yang benar.</small>
                            </div>
                            <div class="form-group text-center mt-5">
                                <button type="submit" class="btn btn-dark btn-lg text-white">
                                    SIMPAN DATA
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="mx-auto mt-5">
                            <button type="submit" class="btn btn-dark btn-lg text-white">
                                <i style="font-size:4em" class="far fa-check-circle"></i>
                                SIMPAN DATA
                            </button>
                        </div>
                    </div> -->  
                </form>
            </div>
            
        </div>
        
  </div>
  
  </div>
    <?php $this->load->view('common/footer_test_view'); ?>
    <?php $this->load->view('common/js_view'); ?>
    <?php echo js('toastr.min.js'); ?>
    <?php
      if($this->session->flashdata('success')){
        ?>
        <script type="text/javascript">
            toastr.success('Data masjid berhasil di tambahkan', 'Masjidpedia');
        </script>
        <?php
      }
      if($this->session->flashdata('error')){
        ?>
        <script type="text/javascript">
            toastr.error('Pastikan anda memasukkan data dengan benar', 'Masjidpedia');
        </script>
        <?php
      }
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBi98DhP4_DO7B59Rnj9wrFgWzwc8dUabc"></script>
    <script type="text/javascript">
        var geocoder = new google.maps.Geocoder();

        function geocodePosition(pos) {
          geocoder.geocode({
            latLng: pos
          }, function(responses) {
            if (responses && responses.length > 0) {
              updateMarkerAddress(responses[0].formatted_address);
            } else {
              updateMarkerAddress('Cannot determine address at this location.');
            }
          });
        }

        // function updateMarkerStatus(str) {
        //   document.getElementById('markerStatus').innerHTML = str;
        // }

        function updateMarkerPosition(latLng) {
          document.getElementById('info').value = [
            latLng.lat(),
            latLng.lng()
          ].join('; ');
        }

        function updateMarkerAddress(str) {
          document.getElementById('address').innerHTML = str;
        }

        function initialize() {
          var latLng = new google.maps.LatLng(-5.147665, 119.432732);
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          });
          var marker = new google.maps.Marker({
            position: latLng,
            title: 'Point A',
            map: map,
            draggable: true,
            zIndex: 55
          });

          // Update current position info.
          updateMarkerPosition(latLng);
          geocodePosition(latLng);

          // Add dragging event listeners.
          google.maps.event.addListener(marker, 'dragstart', function() {
            updateMarkerAddress('Dragging...');
          });

          google.maps.event.addListener(marker, 'drag', function() {
            //updateMarkerStatus('Dragging...');
            updateMarkerPosition(marker.getPosition());
          });

          google.maps.event.addListener(marker, 'dragend', function() {
            //updateMarkerStatus('Drag ended');
            geocodePosition(marker.getPosition());
          });
        }

        // Onload handler to fire off the app.
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</body>
</html>