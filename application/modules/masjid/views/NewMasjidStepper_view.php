<!DOCTYPE html>
<html>
<head>
  <title>Mp | Tambah Masjid</title>
  <?php $this->load->view('common/meta_view'); ?>
  <?php echo css('toastr.min.css'); ?>
  <?php echo css('newmasjid.css'); ?>
  <style type="text/css">
      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        /* margin-left: 12px; */
         padding: 0 11px 0 13px; 
        text-overflow: ellipsis;
        margin-bottom: 5px;
        width: 100%;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }
  </style>
  <style>
    .custom-control-label::before, 
    .custom-control-label::after {
      top: .55rem;
      width: 1.23rem;
      height: 1.23rem;
    }
  </style>
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <a class="text-danger" style="position:absolute;right:7px;top:5px;" href="<?php echo base_url(); ?>logout"><i style="font-size:1.5em;" class="fas fa-power-off p-3"></i></a>
      <h3 style="text-align: center;font-size: 3em;" class="display-4 mx-auto mt-5 mb-5">Tambah Data</h3>
    </div>
    <div style="margin-bottom:31px;" class="row">
      <div class="mx-auto"><a href="<?php echo base_url(); ?>masjid" class="text-danger"><i style="font-size:3em" class="fas fa-chevron-circle-left"></i></a></div>
    </div>

    <div class="container">
      <div style="min-height: 500px;" class="row">
        
        <div class="col-lg-12">
        <form action="<?php echo base_url(); ?>/masjid/collect" method="POST" role="form">
        <small class="form-text text-muted mb-2">
          Pastikan semua inputan terisi dengan benar. Tanda (<strong style="color:red;">*</strong>)
          menandakan inputan tersebut harus diisi.
        </small>
        <div id="accordion">
          <div class="card mb-1">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button style="width: 100%;text-align: left;text-decoration: none;color:black;" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  <span style="font-size: 1.4em;"><i class="fas fa-star mr-2"></i> Informasi Umum Masjid</span>
                </button>
              </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="card-body">
                <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                      <label for="masjidname">Nama Masjid <strong style="color:red;">*</strong></label>
                      <input required name="masjidname" required type="text" class="form-control form-control-lg" id="masjidname" placeholder="Masukkan nama masjid">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                      <label for="email">Email</label>
                      <div class="input-group">
                          <div class="input-group-prepend">
                              <span class="input-group-text"> <i class="fas fa-envelope"></i> </span>
                          </div>
                          <input name="emailmp" type="email" class="form-control form-control-lg" id="email" placeholder="Masukkan Alamat Email">
                      </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                      <label for="rek">Rekening <strong style="color:red;">*</strong></label>
                      <div class="input-group">
                          <div class="input-group-prepend">
                              <span class="input-group-text"> <i class="far fa-credit-card"></i> </span>
                          </div>
                          <input required name="rek" type="text" class="form-control form-control-lg" id="rek" placeholder="Masukkan Rekening Masjid">
                      </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                      <label for="telepon">Telepon <strong style="color:red;">*</strong></label>
                      <div class="input-group">
                          <div class="input-group-prepend">
                              <span class="input-group-text"> <i class="fas fa-phone"></i> </span>
                          </div>
                          <input required name="telepon" type="text" class="form-control form-control-lg" id="telepon" placeholder="Masukkan Telepon">
                      </div>
                  </div>
                </div>
              </div>
              </div>
            </div>
          </div>
          <div class="card mb-1">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button style="width: 100%;text-align: left;text-decoration: none;color:green;" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  <span style="font-size: 1.4em;"><i class="fas fa-map-marker-alt mr-2"></i> Lokasi Masjid</span>
                </button>
              </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                        <label for="address">Pin Lokasi</label>
                        <div class="alert alert-success" role="alert">
                          Pindahkan (drag) pin marker sesuai dengan alamat masjid anda.
                        </div>
                        <div style="height:410px;border:solid 1px grey;width: 100%;" id="map"></div>
                        <small style="margin-top:3px;" class="form-text text-muted">Pindahkan pin marker sesuai dengan alamat masjid anda.</small>
                        <div id="infoPanel">
                            <input id="info" type="hidden" name="kordinat">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address">Alamat <strong style="color:red;">*</strong></label>
                        <textarea required id="address" name="alamat" placeholder="Masukkan Alamat Masjid" class="form-control form-control-lg"></textarea>
                        <small style="margin-top:3px;" class="form-text text-muted">Jika alamat anda tidak sesuai, silahkan ganti dengan alamat yang benar.</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card mb-1">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button style="width: 100%;text-align: left;text-decoration: none;color:#013fa3;" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  <span style="font-size: 1.4em;"><i class="fas fa-tasks mr-2"></i> Fasilitas Masjid</span>
                </button>
              </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-4">
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input name="alquran" type="checkbox" class="custom-control-input" id="alquran">
                        <label style="font-size: 1.4em;margin-left:7px;padding-top:2px;" class="custom-control-label" for="alquran">Al-Quran</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input name="sajadah" type="checkbox" class="custom-control-input" id="sajadah">
                        <label style="font-size: 1.4em;margin-left:7px;padding-top:2px;" class="custom-control-label" for="sajadah">Sajadah</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input name="karpet" type="checkbox" class="custom-control-input" id="karpet">
                        <label style="font-size: 1.4em;margin-left:7px;padding-top:2px;" class="custom-control-label" for="karpet">Karpet Sajadah</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input name="wudhu" id="wudhu" type="checkbox" class="custom-control-input">
                        <label style="font-size: 1.4em;margin-left:7px;padding-top:2px;" class="custom-control-label" for="wudhu">Air Wudhu</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input name="mukenah" id="mukenah" type="checkbox" class="custom-control-input">
                        <label style="font-size: 1.4em;margin-left:7px;padding-top:2px;" class="custom-control-label" for="mukenah">Mukenah</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input name="sarung" id="sarung" type="checkbox" class="custom-control-input">
                        <label style="font-size: 1.4em;margin-left:7px;padding-top:2px;" class="custom-control-label" for="sarung">Sarung</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input name="penitipan" id="penitipan" type="checkbox" class="custom-control-input">
                        <label style="font-size: 1.4em;margin-left:7px;padding-top:2px;" class="custom-control-label" for="penitipan">Penitipan Barang</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input name="loker" id="loker" type="checkbox" class="custom-control-input">
                        <label style="font-size: 1.4em;margin-left:7px;padding-top:2px;" class="custom-control-label" for="loker">Loker Alas Kaki</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input name="ac" id="ac" type="checkbox" class="custom-control-input">
                        <label style="font-size: 1.4em;margin-left:7px;padding-top:2px;" class="custom-control-label" for="ac">Air Conditioner</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input name="kipas" id="kipas" type="checkbox" class="custom-control-input">
                        <label style="font-size: 1.4em;margin-left:7px;padding-top:2px;" class="custom-control-label" for="kipas">Kipas Angin</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input name="toilet" id="toilet" type="checkbox" class="custom-control-input">
                        <label style="font-size: 1.4em;margin-left:7px;padding-top:2px;" class="custom-control-label" for="toilet">Toilet</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card mb-1">
            <div class="card-header" id="headingFour">
              <h5 class="mb-0">
                <button style="width: 100%;text-align: left;text-decoration: none;color:#a20000;" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                  <span style="font-size: 1.4em;"><i class="fas fa-people-carry mr-2"></i> Pengelola Masjid</span>
                </button>
              </h5>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
              <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="ketua">Ketua Pengurus Masjid</label>
                        <input name="ketua" required type="text" class="form-control form-control-lg" placeholder="Masukkan nama ketua masjid">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="wakilketua">Wakil Ketua</label>
                        <input name="wakilketua" required type="text" class="form-control form-control-lg" placeholder="Masukkan nama wakil ketua masjid">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="sekretaris">Sekretaris</label>
                        <input name="sekretaris" required type="text" class="form-control form-control-lg" placeholder="Masukkan nama sekretaris masjid">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="bendahara">Bendahara</label>
                        <input name="bendahara" required type="text" class="form-control form-control-lg" placeholder="Masukkan nama bendahara masjid">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="periode">Periode Kepengurusan</label>
                        <input name="periode" required type="text" class="form-control form-control-lg" placeholder="Masukkan periode kepengurusan">
                        <small style="margin-top:3px;" class="form-text text-muted">Contoh pengisian periode kepengurusan masjid yaitu (2018-2019).</small>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card mb-1">
            <div class="card-header" id="headingFive">
              <h5 class="mb-0">
                <button style="width: 100%;text-align: left;text-decoration: none;color:#b21192;" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                  <span style="font-size: 1.4em;"><i class="fas fa-tablet-alt mr-2"></i> Akun Sosial Media</span>
                </button>
              </h5>
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
              <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                          <label for="web">Website</label>
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text"> <i class="fas fa-globe"></i> </span>
                              </div>
                              <input name="web" type="text" class="form-control form-control-lg" placeholder="Masukkan alamat website masjid">
                          </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                          <label for="fb">Facebook</label>
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text"> <i class="fab fa-facebook"></i> </span>
                              </div>
                              <input name="fb" type="text" class="form-control form-control-lg" placeholder="Masukkan akun facebook masjid">
                          </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                          <label for="tw">Twitter</label>
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text"> <i class="fab fa-twitter"></i> </span>
                              </div>
                              <input name="tw" type="text" class="form-control form-control-lg" placeholder="Masukkan akun twitter masjid">
                          </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                          <label for="yt">Youtube</label>
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text"> <i class="fab fa-youtube"></i> </span>
                              </div>
                              <input name="yt" type="text" class="form-control form-control-lg" placeholder="Masukkan channel youtube masjid">
                          </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingSix">
              <h5 class="mb-0">
                <button style="width: 100%;text-align: left;text-decoration: none;color:#490000;" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                  <span style="font-size: 1.4em;"><i class="fas fa-newspaper mr-2"></i> Deskripsi Masjid</span>
                </button>
              </h5>
            </div>
            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                        <!-- <label for="address">Alamat <strong style="color:red;">*</strong></label> -->
                        <textarea required rows="7" id="deskripsi" name="deskripsi" placeholder="Masukkan Deskripsi Masjid" class="form-control form-control-lg"></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <small class="form-text text-muted mb-2">
            Pastikan semua inputan terisi dengan benar. Tanda (<strong style="color:red;">*</strong>)
            menandakan inputan tersebut harus diisi.
        </small>
        </div>
        <div style="margin-bottom: 85px;" class="col-lg-12">
          <!-- <button type="button" class="btn btn-lg btn-dark">Simpan Data</button> -->
          <div class="form-group text-center mt-5">
              <button type="submit" class="btn btn-dark btn-lg text-white">
                  SIMPAN DATA
              </button>
          </div>
        </form>
        </div>
      </div>
      <!-- <div class="row">
        <div class="col-lg-12">
          <form action="<?php echo base_url(); ?>masjid/test" method="POST" role="form">
            <div class="form-group">
              <div class="custom-control custom-checkbox">
                <input name="quran" type="checkbox" class="custom-control-input" id="quran">
                <label style="font-size: 1.4em;margin-left:7px;padding-top:2px;" class="custom-control-label" for="quran">Al-Quran</label>
              </div>
            </div>
            <div class="form-group mt-5">
              <button type="submit" class="btn btn-warning btn-lg text-white">
                  SIMPAN DATA
              </button>
            </div>
          </form>
        </div>
      </div> -->
    </div> <!-- end container -->
  
  </div>
    <?php $this->load->view('common/footer_test_view'); ?>
    <?php $this->load->view('common/js_view'); ?>
    <?php echo js('toastr.min.js'); ?>
    <?php //echo js('newmasjid.js'); ?>
    <?php
      if($this->session->flashdata('success')){
        ?>
        <script type="text/javascript">
            toastr.success('Data masjid berhasil di tambahkan', 'Masjidpedia');
        </script>
        <?php
      }
      if($this->session->flashdata('error')){
        ?>
        <script type="text/javascript">
            toastr.error('Pastikan anda memasukkan data dengan benar', 'Masjidpedia');
        </script>
        <?php
      }
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBi98DhP4_DO7B59Rnj9wrFgWzwc8dUabc"></script>
    <script type="text/javascript">
        var geocoder = new google.maps.Geocoder();

        function geocodePosition(pos) {
          geocoder.geocode({
            latLng: pos
          }, function(responses) {
            if (responses && responses.length > 0) {
              updateMarkerAddress(responses[0].formatted_address);
            } else {
              updateMarkerAddress('Cannot determine address at this location.');
            }
          });
        }

        // function updateMarkerStatus(str) {
        //   document.getElementById('markerStatus').innerHTML = str;
        // }

        function updateMarkerPosition(latLng) {
          document.getElementById('info').value = [
            latLng.lat(),
            latLng.lng()
          ].join('; ');
        }

        function updateMarkerAddress(str) {
          document.getElementById('address').innerHTML = str;
        }

        function initialize() {
          var latLng = new google.maps.LatLng(-5.147665, 119.432732);
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          });
          var marker = new google.maps.Marker({
            position: latLng,
            title: 'Point A',
            map: map,
            draggable: true,
            zIndex: 55
          });

          // Update current position info.
          updateMarkerPosition(latLng);
          geocodePosition(latLng);

          // Add dragging event listeners.
          google.maps.event.addListener(marker, 'dragstart', function() {
            updateMarkerAddress('Dragging...');
          });

          google.maps.event.addListener(marker, 'drag', function() {
            //updateMarkerStatus('Dragging...');
            updateMarkerPosition(marker.getPosition());
          });

          google.maps.event.addListener(marker, 'dragend', function() {
            //updateMarkerStatus('Drag ended');
            geocodePosition(marker.getPosition());
          });
        }

        // Onload handler to fire off the app.
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</body>
</html>