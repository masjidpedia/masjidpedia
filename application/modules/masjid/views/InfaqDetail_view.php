<!DOCTYPE html>
<html>
<head>
  <title>Mp | Infaq Masjid</title>
  <?php $this->load->view('common/meta_view'); ?>
  <?php echo css('toastr.min.css'); ?>
  <?php echo css('masjid.css'); ?>
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <a class="text-danger" style="position:absolute;right:7px;top:5px;" href="<?php echo base_url(); ?>logout"><i style="font-size:1.5em;" class="fas fa-home p-3"></i></a>
      <h3 style="font-size:3em;" class="display-4 mx-auto mt-5 mb-5">Nama Infaq</h3>
    </div>

    <!-- <div class="row">
      <div class="mx-auto"><a href="<?php echo base_url(); ?>masjid/new" class="text-success"><i style="font-size:4em" class="fab fa-ussunnah"></i></a></div>
    </div> -->

    <div class="container">
        <div style="min-height:350px;" class="row mb-5 pb-5">
          <div class="col-lg-12">
            <div class="row">
              <!-- <div style="background:red;height:400px;width:100%;"></div> -->
              <div class="col-lg-4">
                <div class="row">
                  <div style="background:blue;height:500px;width:100%;"></div>
                </div>
              </div>
              <div class="col-lg-8">
                <div class="row">
                  <div style="background:yellow;height:500px;width:100%;">
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="row">
              <div style="background:red;height:900px;width:100%;">
                
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  <?php $this->load->view('common/footer_test_view'); ?>
  <?php $this->load->view('common/js_view'); ?>
  <?php echo js('toastr.min.js'); ?>
</body>
</html>