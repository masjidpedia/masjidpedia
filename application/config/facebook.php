<?php defined('BASEPATH') OR exit('No direct script access allowed');

$config['facebook_app_id']              = '1208287575862428';
$config['facebook_app_secret']          = 'dfe0a37cfd02fcea4262036a5e5cc618';
$config['facebook_login_type']          = 'web';
$config['facebook_login_redirect_url']  = 'login/fb';
$config['facebook_logout_redirect_url'] = 'logout/fb';
$config['facebook_permissions']         = array('email');
$config['facebook_graph_version']       = 'v2.10';
$config['facebook_auth_on_load']        = TRUE;